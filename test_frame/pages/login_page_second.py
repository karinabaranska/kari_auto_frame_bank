from globals import global_settings


class LoginPageSecond:

    def __init__(self, driver):
        self.base_url = global_settings.base_url
        self.login_url = f'{self.base_url}/logowanie_etap_2.html'
        self.driver = driver

        # exposed XPaths
        self.popup_xpath = '//*[@class="shadowbox-content contact-popup"]/div/h2'
        self.login_button_xpath = '//*[@id="login_next"]'
        self.password_input_xpath = '//*[@id="login_password"]'

    def go_to_page(self):
        driver = self.driver
        driver.get(self.login_url)

    def login_button(self):
        login_next_button_element = self.driver.find_element_by_xpath(self.login_button_xpath)
        return login_next_button_element

    def password_input(self):
        password_input_element = self.driver.find_element_by_xpath(self.password_input_xpath)
        return password_input_element
