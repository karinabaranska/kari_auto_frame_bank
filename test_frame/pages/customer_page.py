from globals import global_settings


class CustomerPage:

    def __init__(self, driver):
        self.base_url = global_settings.base_url
        self.customer_url = f'{self.base_url}/pupit.html'
        self.driver = driver

        # exposed XPaths
        self.messages_element_xpath = '//*[@id="show_messages"]'

    def go_to_page(self):
        driver = self.driver
        driver.get(self.customer_url)
        
    def messages_element(self):
        message_element = self.driver.find_element_by_xpath(self.messages_element_xpath)
        return message_element

    def my_account_button(self):
        account_button_xpath = '/html/body/section/div/div/nav/ul/li[2]/div/a'

        account_button_element = self.driver.find_element_by_xpath(account_button_xpath)
        return account_button_element
