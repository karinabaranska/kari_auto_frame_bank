from globals import global_settings


class AccountPage:

    def __init__(self, driver):
        self.base_url = global_settings.base_url
        self.account_url = f'{self.base_url}/konta.html'
        self.driver = driver

        # exposed XPaths

    def go_to_page(self):
        driver = self.driver
        driver.get(self.account_url)

    def account_header_text(self):
        account_header_xpath = '//*[@id="main_content"]/section/h1'

        account_header_element = self.driver.find_element_by_xpath(account_header_xpath)
        account_header_text = account_header_element.text
        return account_header_text

