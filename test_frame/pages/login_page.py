from globals import global_settings


class LoginPage:

    def __init__(self, driver):
        self.base_url = global_settings.base_url
        self.login_url = f'{self.base_url}/logowanie_etap_1.html'
        self.driver = driver

        # exposed XPaths
        self.login_next_button_xpath = '//*[@id="login_next"]'
        self.popup_xpath = '//*[@class="shadowbox-content contact-popup"]/div/h2'

    def go_to_page(self):
        driver = self.driver
        driver.get(self.login_url)

    def login_header_text(self):
        login_header_xpath = '//*[@id="login_form"]/h1'

        login_form_header_element = self.driver.find_element_by_xpath(login_header_xpath)
        login_form_header_text = login_form_header_element.text
        return login_form_header_text

    def login_input(self):
        login_form_input_xpath = '//*[@id="login_id"]'
        return self.driver.find_element_by_xpath(login_form_input_xpath)

    def clear_login_input(self):
        login_form_input_element = self.login_input()
        login_form_input_element.clear()

    def login_next_button(self):
        login_next_button_element = self.driver.find_element_by_xpath(self.login_next_button_xpath)
        return login_next_button_element

    def login_next_button_state(self):
        login_next_button_element = self.login_next_button()
        button_state = login_next_button_element.get_property('disabled')
        return button_state

    def hint_button(self):
        hint_button_xpath = '//*[@id="login_id_container"]//*[@class="i-hint-white tooltip widget-info"]'
        return self.driver.find_element_by_xpath(hint_button_xpath)

    def warning_message(self):
        warning_message_xpath = '//*[@class="error"]'
        warning_message_element = self.driver.find_element_by_xpath(warning_message_xpath)
        return warning_message_element

    def login_reminder(self):
        login_reminder_xpath = '//*[@id="ident_rem"]'
        login_reminder_element = self.driver.find_element_by_xpath(login_reminder_xpath)
        return login_reminder_element

    def popup(self):
        popup_element = self.driver.find_element_by_xpath(self.popup_xpath)
        return popup_element
