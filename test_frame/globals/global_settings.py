import os

from selenium import webdriver
from selenium.webdriver.support.events import EventFiringWebDriver

from helpers.global_helper import project_path

from helpers.screenshot_listener import ScreenshotListener

base_url = 'https://karina-demo-bank-staging.herokuapp.com/'


def init_driver():
    """
    Starting driver for all tests
    For machines different than local run on Windows (nt) external WebDriver is expected under proper url
    """
    if os.name == 'nt':
        path = project_path()
        print("PATH: ", rf"{path}/libs/chromedriver.exe")
        driver = webdriver.Chrome(executable_path=rf"{path}/libs/chromedriver.exe")
    else:
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        driver = webdriver.Chrome(options=chrome_options)
        driver.set_window_size(1920, 1080)

    ef_driver = EventFiringWebDriver(driver, ScreenshotListener())

    return ef_driver



