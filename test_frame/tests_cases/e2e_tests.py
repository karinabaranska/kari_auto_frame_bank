import unittest

from globals import global_settings as globals
from pages.login_page import LoginPage
from pages.login_page_second import LoginPageSecond
from pages.customer_page import CustomerPage
from pages.account_page import AccountPage
from helpers import operational_helpers as oh
from helpers.wrappers import screenshot_decorator


class E2ETests(unittest.TestCase):

    def setUp(self):
        self.ef_driver = globals.init_driver()
        self.login_page = LoginPage(self.ef_driver)
        self.login_page_second = LoginPageSecond(self.ef_driver)
        self.customer_page = CustomerPage(self.ef_driver)
        self.account_page = AccountPage(self.ef_driver)

    def tearDown(self):
        self.ef_driver.quit()

    @screenshot_decorator
    def test_correct_login_to_customer_page(self):
        expected_messages_text = 'Brak wiadomości'

        self.login_page.go_to_page()
        login_text = '12345678'
        self.login_page.clear_login_input()
        self.login_page.login_input().send_keys(login_text)

        self.login_page.login_next_button().click()

        oh.clickability_of_element_wait(self.ef_driver, self.login_page_second.password_input_xpath)
        self.login_page_second.password_input().send_keys('123456789')
        self.login_page_second.login_button().click()

        oh.visibility_of_element_wait(self.ef_driver, self.customer_page.messages_element_xpath, 10)

        # finding unique element to check if login was successful
        messages = self.customer_page.messages_element()
        actual_messages_text = messages.text
        self.assertEqual(expected_messages_text, actual_messages_text,
                         f'Expected message text differ from actual: {actual_messages_text}')

        self.customer_page.my_account_button().click()
        expected_header_text = 'konta osobiste'

        self.account_page.go_to_page()
        actual_header_text = self.account_page.account_header_text()

        self.assertEqual(expected_header_text, actual_header_text,
                         f'Assert failed on: {self.account_page.account_url}')

