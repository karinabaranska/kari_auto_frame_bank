import unittest

from globals import global_settings as globals
from pages.login_page import LoginPage
from pages.login_page_second import LoginPageSecond
from pages.customer_page import CustomerPage
from helpers import operational_helpers as oh
from helpers.wrappers import screenshot_decorator


class LoginTests(unittest.TestCase):

    def setUp(self):
        self.ef_driver = globals.init_driver()
        self.login_page = LoginPage(self.ef_driver)
        self.login_page_second = LoginPageSecond(self.ef_driver)
        self.customer_page = CustomerPage(self.ef_driver)

    def tearDown(self):
        self.ef_driver.quit()

    @screenshot_decorator
    def test_exact_text_for_login_form_header(self):
        expected_header_text = 'Wersja demonstracyjna serwisu demobank'

        self.login_page.go_to_page()
        login_form_header_text = self.login_page.login_header_text()
        self.assertEqual(expected_header_text, login_form_header_text,
                         f'Assert failed on: {self.login_page.login_url}')

    @screenshot_decorator
    def test_button_still_is_disabled_when_login_input_is_empty(self):
        expected_login_button_disabled = True

        self.login_page.go_to_page()
        self.login_page.clear_login_input()
        actual_login_next_button_disabled = self.login_page.login_next_button_state()

        self.assertEqual(expected_login_button_disabled, actual_login_next_button_disabled,
                         f'Expected state of "dalej" button: True , differ from actual: {actual_login_next_button_disabled} , for page url:  {self.login_page.login_url}')

    @screenshot_decorator
    def test_display_error_message_when_user_submit_less_than_8_signs(self):
        expected_warning_text = 'identyfikator ma min. 8 znaków'

        self.login_page.go_to_page()
        self.login_page.clear_login_input()
        login_text = '1234567'
        self.login_page.login_input().send_keys(login_text)

        hint_button = self.login_page.hint_button()
        hint_button.click()

        warning_message = self.login_page.warning_message()
        actual_warning_message_text = warning_message.text

        self.assertEqual(expected_warning_text, actual_warning_message_text,
                         f'Expected warning message differ from actual one for url: {self.login_page.login_url}')

    @screenshot_decorator
    def test_button_still_respond_when_enters_8_signs_id(self):
        expected_new_login_button_text = 'zaloguj się'

        self.login_page.go_to_page()
        login_text = '12345678'
        self.login_page.clear_login_input()
        self.login_page.login_input().send_keys(login_text)

        login_next_button = self.login_page.login_next_button()
        login_next_button.click()

        oh.clickability_of_element_wait(self.ef_driver, self.login_page.login_next_button_xpath)
        oh.wait_for_elements(self.ef_driver, self.login_page_second.login_button_xpath)

        login_button = self.login_page_second.login_button()
        actual_login_button_text = login_button.text
        self.assertEqual(expected_new_login_button_text, actual_login_button_text,
                         f'Expected login button text: zaloguj się , differ from actual {expected_new_login_button_text}')

    @screenshot_decorator
    def test_correct_popup_text(self):
        expected_popup_text = 'ta funkcja jest niedostępna'

        self.login_page.go_to_page()

        login_reminder = self.login_page.login_reminder()
        login_reminder.click()

        oh.visibility_of_element_wait(self.ef_driver, self.login_page.popup_xpath)

        popup = self.login_page.popup()
        actual_popup_text = popup.text

        self.assertEqual(expected_popup_text, actual_popup_text,
                         f'Expected popup text differ from actual: {actual_popup_text}')


