import unittest

from globals import global_settings as globals
from pages.account_page import AccountPage
from helpers import operational_helpers as oh
from helpers.wrappers import screenshot_decorator


class AccountTests(unittest.TestCase):

    def setUp(self):
        self.ef_driver = globals.init_driver()
        self.account_page = AccountPage(self.ef_driver)

    def tearDown(self):
        self.ef_driver.quit()

    @screenshot_decorator
    def test_exact_text_for_account_header(self):
        expected_header_text = 'konta osobiste'

        self.account_page.go_to_page()
        actual_header_text = self.account_page.account_header_text()

        self.assertEqual(expected_header_text, actual_header_text,
                         f'Assert failed on: {self.account_page.account_url}')
