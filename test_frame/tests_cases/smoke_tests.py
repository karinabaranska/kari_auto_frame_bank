import unittest

from globals import global_settings
from helpers.wrappers import screenshot_decorator


class SmokeTests(unittest.TestCase):

    def setUp(self):
        self.ef_driver = global_settings.init_driver()
        self.base_url = global_settings.base_url
        self.assert_msg = 'Expected title differ from actual for page url:'

    def tearDown(self):
        self.ef_driver.quit()

    @screenshot_decorator
    def test_demo_login(self):
        driver = self.ef_driver
        url = f'{self.base_url}/logowanie_etap_1.html'
        expected_title = 'Demobank - Bankowość Internetowa - Logowanie'

        driver.get(url)
        actual_title = driver.title

        self.assertEqual(expected_title, actual_title,
                         f'{self.assert_msg} {url}')

    @screenshot_decorator
    def test_demo_accounts(self):
        driver = self.ef_driver
        url = f'{self.base_url}/konta.html'
        expected_title = 'Demobank - Bankowość Internetowa - Konta'

        driver.get(url)
        actual_title = driver.title

        self.assertEqual(expected_title, actual_title,
                         f'{self.assert_msg} {url}')

    @screenshot_decorator
    def test_demo_pulpit(self):
        driver = self.ef_driver
        url = f'{self.base_url}/pulpit.html'
        expected_title = 'Demobank - Bankowość Internetowa - Pulpit'

        driver.get(url)
        actual_title = driver.title

        self.assertEqual(expected_title, actual_title,
                         f'{self.assert_msg} {url}')

    @screenshot_decorator
    def test_demo_transfer(self):
        driver = self.ef_driver
        url = f'{self.base_url}/przelew_nowy_zew.html'
        expected_title = 'Demobank - Bankowość Internetowa - Przelew'

        driver.get(url)
        actual_title = driver.title

        self.assertEqual(expected_title, actual_title,
                         f'{self.assert_msg} {url}')
