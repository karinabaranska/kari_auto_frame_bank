import time
import unittest
import imaplib
import email

from selenium.webdriver import ActionChains
from helpers import operational_helpers as oh


def found_items_in_list(driver, list_xpath, expected_number=1):
    """Searching for a new item in the list of things

    :param driver: webdriver instance
    :param list_xpath: xpath of list web element
    :param expected_number: expected number of elements
    """
    items_in_list = oh.wait_for_elements(driver, list_xpath)
    unittest.TestCase().assertEqual(expected_number, len(items_in_list))


def assert_element_image(driver, image_xpath, expected_src):
    """Comparison of the current element's image with the expected element src

    :param driver: webdriver instance
    :param image_xpath: xpath of image element
    :param expected_src: expected src  of element
    """
    image_element = oh.visibility_of_element_wait(driver, image_xpath)
    image_element_src = image_element.get_attribute("src")
    image_end_part_src = image_element_src.endswith(expected_src)
    print(image_end_part_src)
    unittest.TestCase().assertTrue(image_end_part_src, f' Expected {expected_src} '
                                                       f'not included at the end of src attribute, src attribute found: {image_element_src} ')


def assert_element_text(driver, xpath, expected_text):
    """Comparison of the current element's text with the expected element text


    :param driver: webdriver instance
    :param xpath: xpath of web element
    :param expected_text: expected text of element
    """
    element = driver.find_element_by_xpath(xpath)
    element_text = element.text
    unittest.TestCase().assertEqual(expected_text, element_text,
                                    f'Expected text differ from actual on page: {driver.current_url}')


def assert_element_description(driver, xpath, expected_text):
    """Comparison of the current element's text with the expected element text


    :param driver: webdriver instance
    :param xpath: xpath of web element
    :param expected_text: expected text of element
    """
    element = driver.find_element_by_xpath(xpath)
    element_description = element.get_attribute('description')
    unittest.TestCase().assertEqual(expected_text, element_description,
                                    f'Expected text differ from actual on page: {driver.current_url}')


def assert_element_title(driver, xpath, expected_text):
    """Comparison of the current element's text with the expected element text


    :param driver: webdriver instance
    :param xpath: xpath of web element
    :param expected_text: expected text of element
    """
    element = driver.find_element_by_xpath(xpath)
    element_title = element.get_attribute('title')
    unittest.TestCase().assertEqual(expected_text, element_title,
                                    f'Expected text differ from actual on page: {driver.current_url}')


def assert_title(driver, expected_text):
    """comparison of the current page title with the expected title of the page


    :param driver: webdriver instance
    :param expected_text: expected text
    """
    title = driver.title
    print(f'Actual title: {title}')
    unittest.TestCase().assertEqual(expected_text, title,
                                    f'Expected title differ from actual for page url: {driver.current_url}')


def user_login(driver, user_login, user_pass):
    """Login user to website using given email and password


    :param driver: webdriver instance
    :param user_login: user email
    :param user_pass: user password
    :return: None
    """

    # finding login input box and sending value
    login_email_input_element = oh.visibility_of_element_wait(driver, '//input[@id="email"]', 100)
    login_email_input_element.clear()
    login_email_input_element.send_keys(user_login)

    # finding password input box and sending value

    login_input_password_element = driver.find_element_by_xpath('//input[@id="password"]')
    login_input_password_element.send_keys(user_pass)

    # finding button 'SIGN IN'

    button_log_in_element = driver.find_element_by_xpath('//button[@class="login-button btn btn-primary"]')
    button_log_in_element.click()


def use_scroll_to_top_button(driver):
    """
    Use button placed on bottom right of the page to scroll up to top
    Usually used when buttons for saving are invisible because of manipulating on the bottom of the screen
    Currently this function help when tests are conducted on smaller screen that why there is an if
    :param driver: instance of webdriver
    :return: None
    """
    if driver.get_window_size()['height'] < 1000:
        scroll_button = oh.visibility_of_element_wait(driver, '//*[@class="t-Body-topButton"]')
        scroll_button.click()
        time.sleep(2)


def send_keys_to_window(driver, keys=''):
    """
    Send multiple keys to window
    :param driver: driver for window where keys will be send
    :param keys: string with keys sequence ie. "Alicja" OR other Keys ie. Keys.TAB + Keys.TAB, AS a SEQUENCE
    :return: None
    """
    actions = ActionChains(driver)
    actions.send_keys(keys)
    actions.perform()


def read_email_from_gmail():
    ORG_EMAIL = "@***"
    FROM_EMAIL = "***" + ORG_EMAIL
    FROM_PWD = "****"
    SMTP_SERVER = "imap.gmail.com"
    mail = imaplib.IMAP4_SSL(SMTP_SERVER)
    mail.login(FROM_EMAIL, FROM_PWD)
    mail.select('inbox')
    try:
        mail = imaplib.IMAP4_SSL(SMTP_SERVER)
        mail.login(FROM_EMAIL, FROM_PWD)
        mail.select('inbox')
        type, data = mail.search(None, 'ALL')
        mail_ids = data[0]

        id_list = mail_ids.split()
        first_email_id = int(id_list[0])
        latest_email_id = int(id_list[-1])

        # for i in range(latest_email_id,first_email_id, -1):
        typ, data = mail.fetch(bytes(str(latest_email_id), 'utf-8'), '(RFC822)')

        for response_part in data:
            if isinstance(response_part, tuple):
                msg = email.message_from_string(response_part[1].decode('utf-8'))
                email_subject = msg['subject']
                email_from = msg['from']
                email_body = msg.get_payload()
                print('From : ' + email_from + '\n')
                print('Subject : ' + email_subject + '\n')
                for word in email_body:
                    print(type(word))
                    print('Body : ' + word + '\n')

                # for i in reversed(email_body):
                #     body = mail.fetch(i, '(RFC822)')
                #     print(body)


    except Exception as e:
        print(str(e))
