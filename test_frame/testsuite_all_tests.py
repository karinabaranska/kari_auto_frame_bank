import unittest
from tests_cases.login_tests import LoginTests
from tests_cases.smoke_tests import SmokeTests
from tests_cases.e2e_tests import E2ETests
from tests_cases.account_tests import AccountTests


def full_suite():
    test_suite = unittest.TestSuite()
    # adding test classes:
    test_suite.addTest(unittest.makeSuite(LoginTests))
    test_suite.addTest(unittest.makeSuite(SmokeTests))
    test_suite.addTest(unittest.makeSuite(E2ETests))
    test_suite.addTest(unittest.makeSuite(AccountTests))

    return test_suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(full_suite())
