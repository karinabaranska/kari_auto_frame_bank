# 🤖 Demo Bank automation test POC

## Helpful commands for local tests development
Open **test_frame** directory as project root

### First steps:
1. Download this repo
1. Download and install [Python](https://www.python.org/downloads/) to local machine
1. Optional: Add venv to the **test_frame** directory
1. Install requirements `pip install -r requirements.txt`
1. Install or update **Chrome** browser
1. Download and place **chromedriver.exe** in [libs](libs) (more in [libs/README.md](libs/README.md))
1. Run tests locally `python -m pytest testsuite_all_tests.py`

### Instaling requirements on local venv 
`pip install -r requirements.txt`

Alternative for Windows and venv:
`venv\Scripts\pip.exe install -r requirements.txt`

### How to run test localy (example with report)
`python -m pytest testsuite_all_tests.py --alluredir ./results`

Alternative for Windows and venv:
`venv\Scripts\python.exe -m pytest testsuite_all_tests.py --alluredir ./results`

⚠ **WARNING** default configuration run tests against online staging env see: [globals/global_settings.py](globals/global_settings.py)

### How to generate report
`libs\allure-2.11.0\bin\allure serve ./results`





