# Kari Auto Frame Bank
This project persent very simple HTML site (static without database) and automation GUI tests run against this site.

## Page
#### Production:  https://karina-demo-bank.herokuapp.com/
#### Staging: https://karina-demo-bank-staging.herokuapp.com/
### Local: navigate to [demobank](demobank) directory

## Tests
### Tests framework placed in  directory [test_frame](test_frame)
### Test execution example: 
#### Tests pass: [example](https://gitlab.com/karinabaranska/kari_auto_frame_bank/-/pipelines/215022909/test_report)
#### Tests skipped: [example](https://gitlab.com/karinabaranska/kari_auto_frame_bank/-/pipelines/214963304/test_report)
#### Tests fail: [example](https://gitlab.com/karinabaranska/kari_auto_frame_bank/-/pipelines/214962336/test_report)
1. Overview of fails and pass  [example](https://gitlab.com/karinabaranska/kari_auto_frame_bank/-/pipelines/214962336/test_report)
1. Click **View details** to check error
1. Screenshots: [example](https://gitlab.com/karinabaranska/kari_auto_frame_bank/-/jobs/844315197/artifacts/browse/test_frame/test_results/)


## CI/CD
### Deployed environments: [link](https://gitlab.com/karinabaranska/kari_auto_frame_bank/-/environments)
#### Production env history: [link](https://gitlab.com/karinabaranska/kari_auto_frame_bank/-/environments/3326906)
#### Staging env history: [link](https://gitlab.com/karinabaranska/kari_auto_frame_bank/-/environments/3326905)
### Pipelines: [link](https://gitlab.com/karinabaranska/kari_auto_frame_bank/-/pipelines)
### Configuration: [.gitlab-ci.yml](.gitlab-ci.yml)


## Made with 💕 and 
[![alt text](https://con.jaktestowac.pl/wp-content/uploads/brand/jaktestowac_small.png)](https://jaktestowac.pl)


    
