/*global window, $, console */

(function () {
    'use strict';

    // uruchomienie uniform'a na selectach oraz radiobuttonach i checkboxach
    window.manager.define('app:modules:uniform:run', {
        on: function () {
            $('select, input[type="radio"], input[type="checkbox"], input[type="file"]').each(function () {
                var classes, $this = $(this);

                if ($this.attr('class') === 'has-error' || $this.attr('class') === 'is-valid') {
                    classes = $this.attr('class');
                }

                if ($this.data('uniformed') === undefined && !$this.hasClass('nouniform')) {
                    $this.uniform({
                        selectAutoWidth: false,
                        fileButtonHtml: $this.data('placeholder') || 'przeglądaj',
                        fileDefaultHtml: '',
                        wrapperClass: $this.data('classes') || classes || null
                    });
                }
            });
            $.uniform.update();
        }
    });
}());
/*global window, $, console, setTimeout, alert, clearTimeout, document */

(function () {
    'use strict';

	window.manager.define('app:modules:modal:window:show', {
        on: function () {
            var oShadowbox;

            oShadowbox = new window.ShadowBox({
                ajax: 'brak_funkcjonalnosci.html',
                classes: 'contact-popup',
                callback: function () {
                    
                    $(document).on('click', '.shadowbox-close', function (e) {
                        e.preventDefault();
                        $('#shadowbox').remove();
                    });
                }
            });
            return oShadowbox;
        }
    });

    
    
    // obsługa menu mobilnego
    window.manager.define('app:modules:navMobile:menuTrigger:run', {
        on: function () {
            var $trigger, $nav_mobile, $search, $trigger_search;

            $trigger = $('#nav_mobile_trigger');
            $nav_mobile = $('#nav_mobile');
            $trigger_search = $('#search_trigger');
            $search = $('#search_form');

            $trigger.off('click').on('click', function (e) {
                var showMenu;

                e.preventDefault();

                showMenu = function () {
                    $nav_mobile.stop(true, false).animate({
                        left: 0
                    }, function () {
                        $trigger.removeClass('inprogress').addClass('active');
                    });
                };

                if (!$trigger.hasClass('inprogress') && !$trigger_search.hasClass('inprogress')) {
                    $trigger.addClass('inprogress');
                    if ($trigger_search.hasClass('active')) {
                        $search.stop(true, false).animate({
                            left: '-100%'
                        }, function () {
                            $trigger_search.removeClass('inprogress active');
                            showMenu();
                        });
                    } else {
                        if ($trigger.hasClass('active')) {
                            $nav_mobile.stop(true, false).animate({
                                left: '-100%'
                            }, function () {
                                $trigger.removeClass('inprogress active');
                                $nav_mobile.children('ul').css('left', '0');
                            });
                        } else {
                            showMenu();
                        }
                    }
                }
            });

            $('.category').children('a').off('click').on('click', function (e) {
                var $this, $submenu;

                $this = $(this);
                $submenu = $this.siblings('.submenu');

                if ($submenu.length) {
                    e.preventDefault();

                    $submenu.addClass('active').show();
                    $nav_mobile.children('ul').stop(true, false).animate({
                        left: '-=110%'
                    });
                }
            });

            $('.back', $nav_mobile).off('click').on('click', function (e) {
                e.preventDefault();

                $nav_mobile.children('ul').stop(true, false).animate({
                    left: '+=110%'
                }, function () {
                    $('.submenu.active', $nav_mobile).removeClass('active').hide();
                });
            });
        }
    });

    // obsługa wyszukiwarki mobilnej
    window.manager.define('app:modules:navMobile:searchTrigger:run', {
        on: function () {
            var $trigger, $search, $trigger_nav, $nav_mobile, showSearcher;

            $trigger = $('#search_trigger');
            $search = $('#search_form');
            $trigger_nav = $('#nav_mobile_trigger');
            $nav_mobile = $('#nav_mobile');

            showSearcher = function () {
                $search.stop(true, false).animate({
                    left: 0
                }, function () {
                    $trigger.removeClass('inprogress').addClass('active');
                });
            };

            $trigger.off('click').on('click', function (e) {
                e.preventDefault();

                if (!$trigger.hasClass('inprogress') && !$trigger_nav.hasClass('inprogress')) {
                    $trigger.addClass('inprogress');

                    if ($trigger_nav.hasClass('active')) {
                        $nav_mobile.stop(true, false).animate({
                            left: '-100%'
                        }, function () {
                            $trigger_nav.removeClass('inprogress active');
                            showSearcher();
                        });
                    } else {
                        if ($trigger.hasClass('active')) {
                            $search.stop(true, false).animate({
                                left: '-100%'
                            }, function () {
                                $trigger.removeClass('inprogress active');
                                $search.children('ul').css('left', '0');
                            });
                        } else {
                            showSearcher();
                        }
                    }
                }
            });
        }
    });

    // obsługa menu 3 stopnia dla mobile
    window.manager.define('app:modules:navMobile:subMenu:run', {
        on: function () {
            var $trigger;

            $trigger = $('.menu-list');

            $trigger.on('change', function (e) {
                e.preventDefault();

                var href, $this, base;

                if (document.getElementsByTagName('base').length) {
                    base = document.getElementsByTagName('base')[0].getAttribute('href');
                } else {
                    base = '';
                }
                $this = $(this);
                href = $('option:selected', $this).val();

                if (href !== undefined) {
                    window.manager.require('app:modules:ui:showLoader:show');
                    window.location.href = base + href;
                }
            });
        }
    });

    // obsługa menu 3 stopnia dla mobile
    window.manager.define('app:modules:navMobile:secondMenu:run', {
        on: function () {
            var $trigger, $list, $current, $items, width, runTimeout, timeout, $active, is_moving, $clone, $first, $last, base;

            if (document.getElementsByTagName('base').length) {
                base = document.getElementsByTagName('base')[0].getAttribute('href');
            } else {
                base = '';
            }
            width = 93;
            $trigger = $('#mobile_nav_wrapper');
            $list = $('ul', $('#mobile_nav_wrapper'));
            is_moving = false;

            // inicjalizacja
            $current = $('.nav-item.active', $list);
            if ($current.is(':first-child')) {
                (function () {
                    $last = $('li:last-child', $list);
                    $clone = $last.clone();
                    $clone.css('left', 0);
                    $last.remove();
                    $list.prepend($clone);
                }());
            }

            $($current.prev().prevAll().get().reverse()).appendTo($list);
            $items = $('li', $list);
            $list.width($items.length * 100);
            $items.each(function (i, item) {
                $(item).css({
                    left: i * width
                });
            });

            if ($items.length < 3) {
                $('#mobile_nav_prev').remove();
                $('#mobile_nav_next').remove();
            }

            runTimeout = function ($this) {
                if (timeout !== undefined && timeout !== null) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function () {
                    window.location.href = base + $this.attr('href');
                }, 1000);
            };
            $active = $('.nav-item.active', $list);

            $list.on('click', 'a', function (e) {
                var $this;

                $this = $(this);
                if (!$this.hasClass('active')) {
                    e.preventDefault();
                    if (!is_moving) {
                        if ($this.parent().index() < $active.index()) {
                            $trigger.trigger('swiperight');
                        } else if ($this.parent().index() > $active.index()) {
                            $trigger.trigger('swipeleft');
                        } else {
                            window.location.href = base + $this.attr('href');
                        }
                    }
                }
            });

            $('#mobile_nav_prev').off('click').on('click', function (e) {
                e.preventDefault();
                if (!is_moving) {
                    $trigger.trigger('swiperight');
                }
            });

            $('#mobile_nav_next').off('click').on('click', function (e) {
                e.preventDefault();
                if (!is_moving) {
                    $trigger.trigger('swipeleft');
                }
            });

            $trigger.on('swipeleft swiperight', function (e) {
                $active = $('.nav-item.active', $list);
                is_moving = true;
                window.manager.require('app:modules:ui:showLoader:show');
                if (e.type === 'swipeleft') {
                    if ($items.length >= 3) {
                        $first = $('li:first-child', $list);
                        $clone = $first.clone();
                        $clone.css('left', ($items.length) * width);
                        $list.append($clone);

                        $items = $('li', $list);
                        $active.removeClass('active').next().addClass('active');
                        setTimeout(function () {
                            $items.each(function (i, item) {
                                $(item).css({
                                    left: (i - 1) * width
                                });
                            });

                            setTimeout(function () {
                                $first.remove();
                                $items = $('li', $list);
                                is_moving = false;
                            }, 400);
                        }, 10);
                    } else {
                        if ($active.is(':first-child')) {
                            $active.removeClass('active').next().addClass('active');
                            $items.each(function (i, item) {
                                $(item).css({
                                    left: i * width
                                });
                            });

                            setTimeout(function () {
                                $items = $('li', $list);
                                is_moving = false;
                            }, 400);
                        }
                    }
                } else if (e.type === 'swiperight') {
                    if ($items.length >= 3) {
                        $last = $('li:last-child', $list);
                        $clone = $last.clone();
                        $clone.css('left', -1 * width);
                        $list.prepend($clone);

                        $active.removeClass('active').prev().addClass('active');
                        $items = $('li', $list);
                        setTimeout(function () {
                            $items.each(function (i, item) {
                                $(item).css({
                                    left: i * width
                                });
                            });

                            setTimeout(function () {
                                $last.remove();
                                $items = $('li', $list);
                                is_moving = false;
                            }, 400);
                        }, 10);
                    } else {
                        if ($active.is(':last-child')) {
                            $active.removeClass('active').prev().addClass('active');
                            $items.each(function (i, item) {
                                $(item).css({
                                    left: (i + 1) * width
                                });
                            });

                            setTimeout(function () {
                                $items = $('li', $list);
                                is_moving = false;
                            }, 400);
                        }
                    }
                }

                $active = $('.nav-item.active', $list);
                runTimeout.call(this, $('a', $active));
            });
        }
    });
}());
/*global window, $, console, setTimeout, document, isNaN, tinymce, alert */

(function () {
    'use strict';

    window.manager.define('app:modules:forms:run', {
        on: function () {
            if ($('#header_search').length) {
                window.manager.require('app:modules:forms:headerSearch:run');
            }
            window.manager.require('app:modules:forms:numberField:run');
            window.manager.require('app:modules:forms:maskedField:run');
            window.manager.require('app:modules:forms:submitForm:run');
            window.manager.require('app:modules:forms:fieldBlur:run');
            window.manager.require('app:modules:forms:datePicker:run');
            window.manager.require('app:modules:forms:checkAll:run');

            if ($('.formmanager').length) {
                window.manager.require('app:modules:forms:formManager:run');
            }
            if ($('.phone-form').length) {
                window.manager.require('app:modules:forms:phoneForm:run');
            }
            if ($('.load-more').length) {
                window.manager.require('app:modules:forms:loadMore:run');
            }
            if ($('.asynch_save').length) {
                window.manager.require('app:modules:forms:asynchSave:run');
            }
            if ($('.cancel-transfer').length) {
                window.manager.require('app:modules:forms:cancelTransfer:run');
            }
            if ($('.filters-form').length) {
                window.manager.require('app:modules:form:filterForm:run');
            }
            if ($('#offer_form').length) {
                window.manager.require('app:modules:form:offerForm:run');
            }
            if ($('#contact_site_form').length) {
                window.manager.require('app:modules:form:contactSiteForm:run');
            }
            if ($('#avatar_crop').length) {
                window.manager.require('app:modules:forms:avatarCrop:run');
            }
            if ($('#avatar_upload').length) {
                window.manager.require('app:modules:forms:uploadAvatar:run');
            }
            if ($('#change_avatar').length) {
                window.manager.require('app:modules:forms:changeAvatar:run');
            }
        }
    });

    // obsługa pola kwoty, zmiana kropki na przecinek na polach z klasą 'number'
    window.manager.define('app:modules:forms:numberField:run', {
        on: function ($field) {

            $field = $field || $('input.number');
            $field.prop('maxlength', 12);

            $field.on('keydown', function (e) {
                var code, allowed_codes;

                code = e.keyCode || e.charCode;

                allowed_codes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 188, 190, 35, 36, 37, 39, 9, 8, 46,
                    96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110];

                if ($.inArray(code, allowed_codes) === -1) {
                    return false;
                }
            });

            $field.on('keyup', function (e) {
                var $this, text, code;

                code = e.keyCode || e.charCode;

                if (code !== 37 && code !== 39 && code !== 36 && code !== 35) {
                    $this = $(this);
                    text = $this.val();

                    if (text.indexOf('.') !== -1) {
                        text = text.replace(/\./g, ',');
                        $this.val(text);
                    }
                }
            });

            $field.on('blur', function (e) {
                var $this, text;

                $this = $(this);
                text = $this.val();
                if (text !== '') {
                    text = text.replace(/,/g, '.');
                    text = Number(text).toFixed(2);
                    if (!isNaN(text)) {
                        text = text.replace(/\./g, ',');
                        $this.val(text);
                    }
                }
            });
        }
    });

    window.manager.define('app:modules:forms:maskedField:run', {
        on: function () {
			$('input.iban').mask('00 0000 0000 0000 0000 0000 0000');
			$('.mobile-phone').mask('000 000 000');
			$('.landline-phone').mask('00 000 00 00');
			$('.postcode').mask('00-000');
			$('.mask-pesel').mask('00000000000');
			$('input.mask-transfer_token').mask('000 000', {placeholder: "___ ___"});
        }
    });

    // obsługa wysłania formularza - walidacja pól o ile takowa została ustawiona
    window.manager.define('app:modules:forms:submitForm:run', {
        on: function () {
            $('form').submit(function (e) {
                var berror, $this;

                $this = $(this);
                berror = window.oValidator.checkForm({
                    form: $this
                });

                if (berror) {
                    e.preventDefault();
                } else if (!$this.hasClass('load-more-search') && !$this.hasClass('asynch_save') && $this.attr('id') !== 'header_search') {
                    setTimeout(function () {
                        window.manager.require('app:modules:ui:showLoader:show');
                    }, 0);

                    $('input[placeholder]', $this).each(function () {
                        var $field = $(this);
                        if ($field.val() === $field.attr('placeholder')) {
                            $field.val('');
                        }
                    });
                }
            });
        }
    });

    // sprawdzanie walidacji na zejście z inputów
    window.manager.define('app:modules:forms:fieldBlur:run', {
        on: function () {
            $('input, textarea', $('form')).on('blur', function (e) {
                var $this;

                $this = $(this);

                if ($this.attr('type') !== "radio" && $this.attr('type') !== "checkbox" && !$this.hasClass('datepicker')) {
                    if ($this.data('validator') !== undefined) {
                        window.oValidator.checkField({
                            item: $this
                        });
                    }
                }
            });

            $('input.datepicker, input[type="checkbox"], input[type="radio"], select', $('form')).on('change', function (e) {
                var $this;

                $this = $(this);

                if ($this.data('validator') !== undefined) {
                    window.oValidator.checkField({
                        item: $this
                    });
                }
            });
        }
    });

    // uruchomienie datepickera na inputach z klasą 'datepicker'
    window.manager.define('app:modules:forms:datePicker:run', {
        on: function () {
            $('.datepicker').datepicker({
                dateFormat: window.ui_config.dpDateFormat,
                monthNames: window.ui_config.dpMonthNames,
                monthNamesShort: window.ui_config.dpMonthNames,
                dayNamesMin: window.ui_config.dpDayNamesMin,
                firstDay: 1,
                changeMonth: true,
                changeYear: true,
                beforeShow: function () {
                    setTimeout(function () {
                        $('select', $('#ui-datepicker-div')).data('classes', 'selector-small');
                        window.manager.require('app:modules:uniform:run');
                    }, 0);
                },
                onChangeMonthYear: function () {
                    setTimeout(function () {
                        $('select', $('#ui-datepicker-div')).data('classes', 'selector-small');
                        window.manager.require('app:modules:uniform:run');
                    }, 0);
                }
            });

            // trigger dla ikon kalendarza. Musi mieć klasę form-calendar oraz atrybut data-target, który będzie zawierał
            // id (bez hasha) inputa z datepickerem.
            $('.form-calendar').off('click').on('click', function (e) {
                e.preventDefault();

                var $this, id;

                $this = $(this);
                id = $this.data('target');

                $('#' + id).trigger('focus');
            });
        }
    });

    window.manager.define('app:modules:forms:formManager:run', {
        on: function () {
            var $form_manager = $('.formmanager');

            $form_manager.each(function () {
                var oFormManager;

                oFormManager = new window.FormManager($(this).attr('id'));

                return oFormManager;
            });
        }
    });

    window.manager.define('app:modules:forms:phoneForm:run', {
        on: function () {
            var $form_trigger = $('.phone-form');

            $form_trigger.off('click').on('click', function (e) {
                e.preventDefault();

                var $this;

                $this = $(this);

                $this.parentsUntil('.account-item', '.account-details').children('.phone-form-container').slideDown();
            });
        }
    });

    window.manager.define('app:modules:forms:loadMore:run', {
        on: function () {
            var $trigger, loader, $search;

            loader = '<div class="load-more-loader">' +
                '<span class="i-loader">ładowanie</span>' +
                '</div>';

            $trigger = $('.load-more');
            $search = $('.load-more-search');

            $search.submit(function (e) {
                e.preventDefault();

                var $this, $target, target, has_next_id, has_prev_id, $trigger_next, $trigger_previous;

                $this = $(this);
                target = $this.data('target');
                has_next_id = '#' + target.replace('section_', '') + '_has_next';
                has_prev_id = '#' + target.replace('section_', '') + '_has_previous';
                $target = $('#' + target);
                $trigger_next = $('#' + target.replace('section_', '') + '_next');
                $trigger_previous = $('#' + target.replace('section_', '') + '_previous');


                if (!$('.has-error, .range-error', $(this)).length) {
                    $target.append(loader);

                    $.post($this.attr('action'), $this.serialize(), function (response) {
                        $target.html(response);

                        if ($(has_next_id).val() === 'true') {
                            $trigger_next.show();
                        } else {
                            $trigger_next.hide();
                        }

                        if ($(has_prev_id).val() === 'true') {
                            $trigger_previous.show();
                        } else {
                            $trigger_previous.hide();
                        }
                    });
                }
            });

            $trigger.off('click').on('click', function (e) {
                e.preventDefault();

                var $this, type, $target, target, offset, has_next_id, has_prev_id, $trigger_next, $trigger_previous;

                $this = $(this);
                type = $this.data('type');
                target = $this.data('target');
                has_next_id = '#' + target.replace('section_', '') + '_has_next';
                has_prev_id = '#' + target.replace('section_', '') + '_has_previous';
                offset = parseInt($('#' + target.replace('section_', '') + '_offset').val(), 10);
                $target = $('#' + target);
                $trigger_next = $('#' + target.replace('section_', '') + '_next');
                $trigger_previous = $('#' + target.replace('section_', '') + '_previous');

                if (type === 'next') {
                    offset += 1;
                } else {
                    offset -= 1;
                }

                $target.append(loader);
                $.get($this.attr('href') + '?offset=' + offset, function (response) {
                    $target.html(response);

                    if ($(has_next_id).val() === 'true') {
                        $trigger_next.show();
                    } else {
                        $trigger_next.hide();
                    }

                    if ($(has_prev_id).val() === 'true') {
                        $trigger_previous.show();
                    } else {
                        $trigger_previous.hide();
                    }
                });
            });
        }
    });

    window.manager.define('app:modules:forms:asynchSave:run', {
        on: function () {
            var $form = $('.asynch_save');

            $form.each(function () {
                var $this, berror;

                $this = $(this);

                $this.submit(function (e) {
                    e.preventDefault();

                    var $flash, $submit, autohide, printResponse, showError;

                    autohide = function ($this, $flash) {
                        if ($this.data('after') !== undefined) {
                            if ($this.data('after') === 'close') {
                                $this.parent().animate({
                                    opacity: 0
                                }, function () {
                                    $(this).slideUp(function () {
                                        $(this).remove();
                                    });
                                });
                            }
                            if ($this.data('after') === 'phone') {
                                $this.parent().slideUp(function () {
                                    $flash.slideUp(function () {
                                        $(this).remove();
                                    });
                                });
                            }
                        } else {
                            $flash.slideUp(function () {
                                $(this).remove();
                            });
                        }
                    };

                    printResponse = function (response, $this) {
                        var status, msgs, statuses;

                        if (typeof response === "object") {

                            try {
                                status = response.kod; // 0 sukces, 1 ostrzeżenie, 2 info, -1 błąd
                                msgs = response.komunikat.reverse(); // array z komunikatami
                                statuses = {
                                    '0': ['flash-success', 'i-confirmed'],
                                    '1': ['flash-info', 'i-warning-red'],
                                    '2': ['flash-info', 'i-warning'],
                                    '-1': ['flash-warning', 'i-warning']
                                };

                                msgs.forEach(function (msg) {
                                    $this.prepend('<div class="hide flash-msg ' + statuses[status][0] + '">' +
                                        '<i class="' + statuses[status][1] + '"></i>' +
                                        msg +
                                        '</div>');
                                });
                            } catch (e) {
                                showError.call(this, $this);
                            }

                        } else {
                            if ($this.data('message') === undefined) {
                                $this.prepend('<div class="hide flash-msg flash-warning">' +
                                    '<i class="i-warning"></i>' +
                                    ($this.data('error') || 'wystąpił nieoczekiwany błąd') +
                                    '</div>');
                            } else {
                                $this.prepend('<div class="hide flash-msg flash-saved">' +
                                    '<i class="i-confirmed"></i>' +
                                    $this.data('message') +
                                    '</div>');
                            }
                        }

                        $flash = $('.flash-msg', $this);
                        $flash.slideDown();

                        setTimeout(function () {
                            autohide.call(this, $this, $flash);
                        }, 3000);

                    };

                    showError = function ($this) {
                        var $flash_error;
                        $this.prepend('<div class="hide flash-msg flash-warning">' +
                            '<i class="i-warning"></i>' +
                            'wystąpił nieoczekiwany błąd' +
                            '</div>');

                        $flash_error = $('.flash-msg', $this);
                        $flash_error.slideDown();

                        setTimeout(function () {
                            autohide.call(this, $this, $flash_error);
                        }, 3000);
                    };

                    berror = false;
                    berror = window.oValidator.checkForm({
                        form: $this
                    });

                    if (!berror) {
                        $submit = $('[type="submit"]', $this);
                        $submit.prop('disabled', true);
                        $flash = $('.flash-msg', $this);

                        if ($flash.length) {
                            $flash.slideUp(100, function () {
                                $(this).remove();
                            });
                        }

                        if ($this.attr('method') === 'post') {
                            $.post($this.attr('action'), $this.serialize(), function (response) {

                                printResponse.call(this, response, $this);

                            }).fail(function () {

                                showError.call(this, $this, $flash);

                            }).always(function () {

                                $submit.prop('disabled', false);

                            });
                        } else if ($this.attr('method') === 'get') {
                            $.get($this.attr('action') + '?' + $this.serialize(), function (response) {

                                printResponse.call(this, response, $this);

                            }).fail(function () {

                                showError.call(this, $this, $flash);

                            }).always(function () {

                                $submit.prop('disabled', false);

                            });
                        }
                    }
                });
            });
        }
    });

    window.manager.define('app:modules:forms:cancelTransfer:run', {
        on: function () {
            $(document).on('click', '.cancel-transfer', function (e) {
                e.preventDefault();

                var $this;

                $this = $(this);

                window.manager.require('app:modules:ui:showDialog:run', this, 'confirm', 'wycofanie przelewu',
                    'czy na pewno chcesz wycofać (odrzucić) przelew (nie zostanie zrealizowany)?', $this.attr('href'),
                    $this.parentsUntil('body', '.history-list-item'), function () {
                        window.manager.require('app:modules:accounts:history:transactionsResults:run');
                    });
            });
        }
    });

    window.manager.define('app:modules:form:offerForm:run', {
        on: function () {
            //wybór typów ofert
            var $container = $('#form_offer_types'),
                $inputs = $container.find('input[type="checkbox"]'),
                $no_offer = $('#form_no_offer');

            $inputs.on('change', function () {
                if ($no_offer.prop('checked') === true) {
                    $inputs.prop('checked', false).closest('label').css('color', '#ccc');
                    $no_offer.prop('checked', true).closest('label').css('color', '#000');
                    $.uniform.update();
                } else {
                    $inputs.closest('label').css('color', 'inherit');
                    $.uniform.update();
                }
            });

            //ukrycie listy do określania wieku dzicka w wypadku braku dzieci
            $('#child_number').on('change', function () {
                if ($(this).val() === '0') {
                    $('#oldest_child_container').slideUp();
                } else {
                    $('#oldest_child_container').slideDown();
                }
            });
        }
    });

    window.manager.define('app:modules:forms:headerSearch:run', {
        on: function () {
            var $form, $trigger, page, ac, mouseFlag, origClose, base;

            if (document.getElementsByTagName('base').length) {
                base = document.getElementsByTagName('base')[0].getAttribute('href');
            } else {
                base = '';
            }
            page = 1;

            $form = $('#header_search');
            $trigger = $('input[type="search"]', $form);
            if (!$trigger.length) {
                $trigger = $('input[type="text"]', $form);
            }

            $trigger.autocomplete({
                closeOnSelect: false,
                minLength: 3,
                source: function (request, callback) {
                    $.get('search.json?query=' + $trigger.val() + '&page=' + page, function (response) {
                        if (typeof response !== 'object') {
                            response = $.parseJSON(response);
                        }

                        response.results.push({
                            string: 'wyników: ' + response.additionalResults,
                            value: $trigger.val(),
                            action: 'next_results'
                        });

                        callback.call(this, $.map(response.results, function (item) {
                            return $.extend(item, {
                                label: item.string,
                                value: item.value || item.string.replace(/(<([^>]+)>)/ig, "")
                            });
                        }));
                    });
                },
                select: function (event, ui) {
                    if (ui.item.action !== undefined && ui.item.action === 'next_results') {
                        page++;
                        if ($trigger.data('ui-autocomplete').bindings[1] !== undefined) {
                            if ($($trigger.data('ui-autocomplete').bindings[1]).length) {
                                $($trigger.data('ui-autocomplete').bindings[1]).append('<span class="loading"></span>');
                            }
                        }
                        $trigger.autocomplete('search', $trigger.val());
                    } else {
                        if (ui.item.newWindow) {
                            window.open(ui.item.url);
                        } else {
                            window.manager.require('app:modules:ui:showLoader:show');
                            window.location.href = base + ui.item.url;
                        }
                    }
                    return false;

                },
                close: function (event, ui) {
                    page = 1;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                if (item.action !== undefined && item.action === 'next_results') {
                    return $('<li class="text-right">')
                        .append('<a>' + item.label + '</a>')
                        .appendTo(ul);
                }
                return $("<li>")
                    .append('<a><span class="right">' + item.type + '</span>' + item.label + '</a>')
                    .appendTo(ul);
            };

            ac = $trigger.data('ui-autocomplete');
            origClose = ac.close;
            mouseFlag = false;
            ac.menu.element.on('mousedown', function () {
                mouseFlag = true;
            });
            ac.close = function (e) {
                if (!mouseFlag) {
                    origClose.apply(this, arguments);
                } else {
                    // clean flag in setTimeout to avoid async events
                    setTimeout(function () {
                        mouseFlag = false;
                    }, 1);
                }
            };


            $form.submit(function (e) {
                e.preventDefault();

                $trigger.autocomplete('search', $trigger.val());
                $trigger.focus();
            });
        }
    });

    window.manager.define('app:modules:form:filterForm:run', {
        on: function () {
            var dateRangeStart = $('#history_range_start').val(),
                dateRangeEnd = $('#history_range_end').val(),
                $historyEnd,
                $historyStart;

            $('#filter_amount_min, #filter_amount_max').val('');

            $('#history_filter_advanced_reset').on('click', function (e) {
                e.preventDefault();
                $('#filter_amount_min, #filter_amount_max').val('').trigger('focus').trigger('blur');
                $('#history_range_start').val(dateRangeStart);
                $('#history_range_end').val(dateRangeEnd);
                $.uniform.update();
            });

            $historyStart = $('#history_range_start');
            $historyStart.datepicker('option', 'maxDate', dateRangeEnd);
            $historyStart.datepicker('option', 'onClose', function (selectedDate) {
                $("#history_range_end").datepicker("option", "minDate", selectedDate);
            });

            //inicjalizacja datepickera dla daty końcowej zakresu historii transakcji
            $historyEnd = $('#history_range_end');
            $historyEnd.datepicker('option', 'maxDate', 0);
            $historyEnd.datepicker('option', 'minDate', dateRangeStart);
            $historyEnd.datepicker('option', 'onClose', function (selectedDate) {
                $("#history_range_start").datepicker("option", "maxDate", selectedDate);
            });

            $('#filter_amount_min, #filter_amount_max').on('blur', function (e) {
                var filterMin = $('#filter_amount_min'),
                    filterMax = $('#filter_amount_max'),
                    amountMin = parseFloat(filterMin.val().replace(',', '.'), 10).toFixed(2),
                    amountMax = parseFloat(filterMax.val().replace(',', '.'), 10).toFixed(2);

                if (isNaN(amountMin)) {
                    amountMin = 0;
                }

                $('#filter_amount_min').val(filterMin.val().replace('.', ','));
                $('#filter_amount_max').val(filterMax.val().replace('.', ','));

                $('#history_account_filters').addClass('disabled').removeClass('active');
                filterMin.closest('.field').removeClass('range-error');
                filterMax.closest('.field').removeClass('range-error').find('.filter-range-error').remove();

                if (Number(amountMin) > Number(amountMax)) {
                    filterMin.closest('.field').addClass('range-error').removeClass('is-valid');
                    filterMax.closest('.field').addClass('range-error').removeClass('is-valid').append('<span class="filter-range-error ms-text-right">nieprawidłowy zakres kwot</span>');
                } else {
                    $('#history_account_filters').removeClass('disabled').addClass('active');
                }
            });
        }
    });

    window.manager.define('app:modules:form:contactSiteForm:run', {
        on: function () {
            var $contactForm = $('#contact_site_form'),
                $submitBtn = $('button', $contactForm);

            $submitBtn.on('click', function (e) {
                e.preventDefault();
                setTimeout(function () {
                    if (!$('.has-error').length) {
                        $contactForm.trigger('submit');
                    }
                }, 500);
            });

            tinymce.init({
                selector: "textarea#contact_site_textarea",
                external_plugins: {
                    "paste": "plugins/paste/plugin.min.js"
                },
                paste_as_text: true,
                menubar : false,
                statusbar : false,
                toolbar: "bold italic underline",
                autosave_ask_before_unload: false,
                height : 140,
                skin_url: 'js/vendor/tinymce/skins',
                setup: function (editor) {
                    editor.on('keyup', function (e) {
                        $('#contact_site_textarea').text(editor.getContent());
                        $('#contact_site_textarea').trigger('change');
                    });
                }
            });

            $('#contact_site_textarea').on('change', function () {
                var $field;

                $field = $(this);
                if ($field.data('validator') !== undefined) {
                    window.oValidator.checkField({
                        item: $field
                    });
                }
            });
        }
    });

    window.manager.define('app:modules:forms:checkAll:run', {
        on: function () {
            var $trigger;

            $trigger = $('.check-all');

            $trigger.on('change', function () {
                var $this, $checkboxes;

                $this = $(this);
                $checkboxes = $('input[name=' + $this.data('checkboxes') + ']');

                if ($this.is(':checked')) {
                    $checkboxes.prop('checked', true);
                } else {
                    $checkboxes.prop('checked', false);
                }

                window.manager.require('app:modules:uniform:run');

                $checkboxes.trigger('change');
            });

        }
    });

    window.manager.define('app:modules:forms:avatarCrop:run', {
    	on: function () {
    		$('#avatar_crop').Jcrop({
                    aspectRatio: 1,
                    setSelect: [0, 0, 160, 160],
                    minSize: [160, 160]
                }
    		);
    	}
    });

    window.manager.define('app:modules:forms:uploadAvatar:run', {
        on: function () {
            var $trigger;

            $trigger = $('#avatar_upload');

            $trigger.on('change', function (e) {
                $(this).parentsUntil('body', 'form').submit();
            });
        }
    });

    window.manager.define('app:modules:forms:changeAvatar:run', {
        on: function () {
            var $trigger, $placer, $chosen;

            $trigger = $('img', $('#change_avatar'));
            $placer = $('#avatar_placer > img');
            $chosen = $('#chosen_avatar');

            $trigger.on('click', function (e) {
                var $this;
                var $placer_src;
                var $placer_typ;
                $this = $(this);
                $placer_src=$placer.attr('src');
                $placer_typ=$placer.data('typ');
                
                $placer.attr('src', $this.attr('src'));
                $placer.data('typ', $this.data('typ'));
                
                $this.attr('src', $placer_src);
                $this.data('typ', $placer_typ);
                $chosen.val($placer.data('typ'));
            });
        }
    });
}());
/*global window, $, console, document, alert, ShadowBox, setTimeout */

(function () {
    'use strict';

    window.countdown_timer = null;

    // uruchomienie modułów odpowiedzialnych za UI.
    window.manager.define('app:modules:ui:run', {
        on: function () {
            window.manager.require('app:modules:ui:tooltip');
            window.manager.require('app:modules:ui:messages');
            window.manager.require('app:modules:ui:lastSession');
            if ($('#session_time').length) {
                window.manager.require('app:modules:ui:sessionTimer');
            }
            window.manager.require('app:modules:ui:showhide:run');
            window.manager.require('app:modules:ui:toggle:run');

            if ($('#aggregation_list').length) {
                window.manager.require('app:modules:ui:aggregationList:run');
            }

            if ($('.table-toggle').length) {
                window.manager.require('app:modules:ui:tableToggle:run');
            }

            if ($('.table-redirect').length) {
                window.manager.require('app:modules:ui:tableRedirect:run');
            }

            if ($('#onboarding, #onboarding_2, #onboarding_3, #onboarding_4').length) {
                window.manager.require('app:modules:ui:onboarding:run');
            }

            window.manager.require('app:modules:ui:showLoader:run');
            window.manager.require('app:modules:ui:slideTo:run');
        }
    });

    // obsługa tooltipa
    window.manager.define('app:modules:ui:tooltip', {
        on: function () {
            // fix for iOS
            $('.tooltip').on('click', function () {
                return true;
            });

            $(document).tooltip({
                items: ".tooltip",
                position: {
                    at: 'right top',
                    my: 'right+20 bottom'
//                    GM: wywalam, ze wzgledu na usuniecie strzalki
//                    my: 'right+20 top+10'
                },
                collision: 'flipfit',
                content: function () {
                    var $this = $(this);
                    return $this.children('.info').html();
                }
            });

            $(document).on('touchend', function () {
                $('.ui-tooltip').fadeOut(function () {
                    var $active;

                    $active = $('.tooltip[aria-describedby]');
                    $active.removeData("ui-tooltip-open");
                    $active.removeData("ui-tooltip-id");
                    $active.removeAttr('aria-describedby');
                    $(this).remove();
                });
            });
        }
    });

    // pokazywanie listy wiadomości
    window.manager.define('app:modules:ui:messages', {
        on: function () {
            var $trigger, $container;

            $trigger = $('#show_messages');
            $container = $('#messages_list');

            $trigger.off('click').on('click', function (e) {
                e.preventDefault();

                if ($container.is(':visible')) {
                    $container.fadeOut();
                } else {
                    $container.css({
                        left: $trigger.offset().left - 25,
                        top: $trigger.offset().top + 30
                    }).fadeIn();
                }
            });

            $(document).on('click touchend', function (e) {
                if ($container.has(e.target).length === 0 && $('#user_messages').has(e.target).length === 0 && $(e.target).attr('id') !== $container.attr('id')) {
                    if ($container.is(':visible')) {
                        $container.fadeOut();
                    }
                }
            });
        }
    });

    // delegacja pokazywania informacji o ostatnim poprawnym i niepoprawnym zalogowaniu
    window.manager.define('app:modules:ui:lastSession', {
        on: function () {
            var $trigger, $container;

            $trigger = $('#show_session');
            $container = $('#last_session');

            $trigger.off('click').on('click', function (e) {
                e.preventDefault();

                if ($container.is(':visible')) {
                    $container.fadeOut();
                } else {
                    $container.css({
                        left: $trigger.offset().left - 277,
                        top: $trigger.offset().top + 30
                    }).fadeIn();
                }
            });

            $(document).on('click touchend', function (e) {
                if ($container.has(e.target).length === 0 && $(e.target).attr('id') !== $container.attr('id') && $trigger[0] !== e.target) {
                    if ($container.is(':visible')) {
                        $container.fadeOut();
                    }
                }
            });
        }
    });

    // uruchomienie działania licznika sesji
    window.manager.define('app:modules:ui:sessionTimer', {
        on: function () {
            var time, $container, countdown_params;

            $container = $('#session_time');
            time = $container.data('time');
            countdown_params = {
                type: "time",
                time: time,
                container: 'session_time',
                callback: function () {
                    window.location.href = 'logowanie_etap_1.html';
                    return true;
                },
                runBeforeEnd: function () {
                    window.manager.require('app:modules:ui:sessionPopup');
                }
            };

            $container.empty();

            if (window.countdown_timer === null) {
                window.countdown_timer = new window.Countdown(countdown_params);
            } else {
                window.countdown_timer.init();
            }
        }
    });

    window.manager.define('app:modules:ui:showLoader:show', {
        on: function ($container) {

            if ($container === undefined) {
                $container = $('#main_content');
            }

            $container.empty();
//            $container.append('<div class="page-loader" />');
            $('#page_loader').show();
        }
    });

    window.manager.define('app:modules:ui:showLoader:run', {
        on: function () {
            var $container;

            $container = $('#main_content');
            $container.after('<div class="page-loader" id="page_loader"/>');
            $('#page_loader').hide();

            $(document).on('click', '.show-page-loader', function (e) {
                var base;

                if (document.getElementsByTagName('base').length) {
                    base = document.getElementsByTagName('base')[0].getAttribute('href');
                } else {
                    base = '';
                }

                window.manager.require('app:modules:ui:showLoader:show', $container);

                if ($(this).attr('href') !== undefined) {
                    e.preventDefault();
                    window.location.href = base + $(this).attr('href');
                }
            });
        }
    });

    // pokazywanie popup'a z informacją o wygaśnięciu sesji.
    window.manager.define('app:modules:ui:sessionPopup', {
        on: function () {
            var oShadowbox;

            oShadowbox = new window.ShadowBox({
                content: '<h2>za <span id="popup_countdown_seconds">30</span> sekund nastąpi automatyczne wylogowanie z serwisu transakcyjnego</h2>' +
                    '<a href="#" id="renew_session" class="btn red arrow">przedłuż sesję</a>',
                classes: 'renew-session',
                callback: function () {
                    var that;

                    that = this;

                    $('#renew_session').off('click').on('click', function (e) {
                        e.preventDefault();

                        $.get('/bezpieczenstwo/ping', function () {
                            return true;
                        });
                        window.manager.require('app:modules:ui:sessionTimer');

                        that.close();
                    });
                }
            });

            return oShadowbox;
        }
    });

    window.manager.define('app:modules:ui:showhide:run', {
        on: function () {
            var $trigger, handler;

            $trigger = $('.showhide');

            handler = function ($this, action, negation) {
                var target, $target, classes, texts;

                target = $this.data('target');
                classes = {
                    show: $this.data('class-show'),
                    hide: $this.data('class-hide')
                };
                texts = {
                    show: $this.data('text-show') || null,
                    hide: $this.data('text-hide') || null
                };
                $target = $('#' + target);

                if ($target.length) {
                    if (action !== undefined) {
                        if (action === 'show') {
                            $target.slideUp();
                            $('input', $target).each(function () {
                                if ($(this).data('no-disabled') === undefined) {
                                    $(this).prop('disabled', true);
                                    window.manager.require('app:modules:uniform:run');
                                }
                            });
                            $this.removeClass(classes.hide).addClass(classes.show);
                            if (texts.show !== null) {
                                $this.text(texts.show);
                            }
                        } else if (action === 'hide') {
                            $target.slideDown();
                            $('input', $target).each(function () {
                                if ($(this).data('no-disabled') === undefined) {
                                    $(this).prop('disabled', false);
                                    window.manager.require('app:modules:uniform:run');
                                }
                            });
                            $this.addClass(classes.hide).removeClass(classes.show);
                            if (texts.hide !== null) {
                                $this.text(texts.hide);
                            }
                        }
                    } else {
                        if ($target.is(':visible')) {
                            $target.slideUp();
                            $('input', $target).each(function () {
                                if ($(this).data('no-disabled') === undefined) {
                                    $(this).prop('disabled', true);
                                    window.manager.require('app:modules:uniform:run');
                                }
                            });
                            $this.removeClass(classes.hide).addClass(classes.show);
                            if (texts.show !== null) {
                                $this.text(texts.show);
                            }
                        } else {
                            $target.slideDown();
                            $('input', $target).each(function () {
                                if ($(this).data('no-disabled') === undefined) {
                                    $(this).prop('disabled', false);
                                    window.manager.require('app:modules:uniform:run');
                                }
                            });
                            $this.addClass(classes.hide).removeClass(classes.show);
                            if (texts.hide !== null) {
                                $this.text(texts.hide);
                            }
                        }
                    }
                }
            };

            $trigger.on('click change', function (e) {
                var $this, negation;

                $this = $(this);
                negation = $this.data('showhide') === 'negation';

                if ($this[0].nodeName.toLowerCase() === "input") {
                    if ($this.is(':checked')) {
                        handler.call(this, $this, negation ? 'show' : 'hide', negation);
                    } else {
                        handler.call(this, $this, negation ? 'hide' : 'show', negation);
                    }
                } else {
                    e.preventDefault();

                    handler.call(this, $this);
                }
            });
        }
    });

    window.manager.define('app:modules:ui:toggle:run', {
        on: function () {
            var $trigger;

            $trigger = $('.toggle');

            $trigger.on('change click', function () {
                var $this, $disable, $target;

                $this = $(this);

                if ($this.data('animate') === 'hard') {
                    $disable = $('div[data-type=' + $this.data('type') + ']');
                    $target = $('#' + $this.data('target'));
                    $disable.hide();
                    $target.show();
                    $('input', $disable).each(function () {
                        if ($(this).data('no-disabled') === undefined) {
                            $(this).prop('disabled', true);
                            window.manager.require('app:modules:uniform:run');
                        }
                    });
                    $('input', $target).each(function () {
                        if ($(this).data('no-disabled') === undefined) {
                            $(this).prop('disabled', false);
                            window.manager.require('app:modules:uniform:run');
                        }
                    });
                } else {
                    $disable = $('div[data-type=' + $this.data('type') + ']:not(#' + $this.data('target') + ')');
                    $target = $('#' + $this.data('target'));
                    $disable.slideUp();
                    $target.slideDown();
                    $('input', $disable).each(function () {
                        if ($(this).data('no-disabled') === undefined) {
                            $(this).prop('disabled', true);
                            window.manager.require('app:modules:uniform:run');
                        }
                    });
//                    console.log($(this).data('no-disabled'));
                    $('input', $target).each(function () {
                        if ($(this).data('no-disabled') === undefined) {
                            $(this).prop('disabled', false);
                            window.manager.require('app:modules:uniform:run');
                        }
                    });
                }
            });
        }
    });

    window.manager.define('app:modules:ui:aggregationList:run', {
        on: function () {
            var $container, showWidgets, slideHandler;
            showWidgets = function ($target) {
                $target.siblings('.account-details').stop(true, false).slideDown(500);
                $target.addClass('active');
//                if ($(window).scrollTop() > $('#aggregation_list').offset().top - 20) {
//                    $('html, body').animate({
//                        scrollTop: $('#aggregation_list').offset().top - 20
//                    }, 500);
//                }
            };

            slideHandler = function (event, toggler) {
                event.preventDefault();

                var $details_visible = $('.account-details:visible', $container);

                if (!toggler.siblings('.account-details').is(':visible')) {
                    if ($details_visible.length) {
                        $details_visible.stop(true, false).slideUp(500);
                        $details_visible.siblings('.account-header').removeClass('active');
                        showWidgets.call(this, toggler);
                    } else {
                        showWidgets.call(this, toggler);
                    }
                } else {
                    $details_visible.stop(true, false).slideUp(500);
                    $details_visible.siblings('.account-header').removeClass('active');
                }
            };

            $container = $('#aggregation_list');

            $('.account-header', $container).off('click').on('click', function (e) {
                var $this, window_width, base;

                if (document.getElementsByTagName('base').length) {
                    base = document.getElementsByTagName('base')[0].getAttribute('href');
                } else {
                    base = '';
                }

                window_width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
                $this = $(this);

                if (window_width < 768) {
                    if ($(e.target).attr('href') === undefined) {
                        if ($this.data('mobilehref') !== undefined) {
                            window.manager.require('app:modules:ui:showLoader:show');
                            window.location.href = base + $this.data('mobilehref');
                        } else if ($this.data('href') !== undefined) {
                            window.manager.require('app:modules:ui:showLoader:show');
                            window.location.href = base + $this.data('href');
                        } else {
                            slideHandler.call(this, e, $this);
                        }
                    }
                } else {
                    if ($(e.target).attr('href') === undefined) {
                        if ($this.data('href') === undefined) {
                            slideHandler.call(this, e, $this);
                        } else {
                            window.manager.require('app:modules:ui:showLoader:show');
                            window.location.href = base + $this.data('href');
                        }
                    }
                }
            });
        }
    });

    window.manager.define('app:modules:moreDetails:run', {
        on: function () {
            var $info = $('#account_is_default_info'),
                $inputs = $('input', '#account_is_default'),
                url = $info.data('url'),
                data = $('#account_is_default').data('url');

            // po kliknięciu w input typu radio wysyłane jest zapytanie na adres wskazany w atrybucie data-url kontenera
            // po błędzie połączenia lub odpowiedzi 400 wyświetla się komunikat o błędzie
            $inputs.on('click', function () {
                $info.text('');

                data = {
                    account_is_default : $('input:checked', '#account_is_default').val()
                };

                $.post(url, data).success(function () {
                    $info.text('');
                }).fail(function () {
                    $info.text('wystąpił błąd');
                });
            });
        }
    });

    window.manager.define('app:modules:ui:tableToggle:run', {
        on: function () {
            var $container, showWidgets, slideHandler;
            showWidgets = function ($target) {
                $target.siblings('.table-details').stop(true, false).slideDown();
                $target.addClass('active');
            };

            slideHandler = function (event, toggler) {
                event.preventDefault();

                var $details_visible = $('.table-details:visible', $container);

                if (!toggler.siblings('.table-details').is(':visible')) {
                    if ($details_visible.length) {
                        $details_visible.stop(true, false).slideUp();
                        $details_visible.siblings('.table-header').removeClass('active');
                        showWidgets.call(this, toggler);
                    } else {
                        showWidgets.call(this, toggler);
                    }
                } else {
                    $details_visible.stop(true, false).slideUp();
                    $details_visible.siblings('.table-header').removeClass('active');
                }
            };

            $container = $('.aggregate-section');

            $container.on('click', '.table-header', function (e) {
                var $this, window_width, base;

                if (document.getElementsByTagName('base').length) {
                    base = document.getElementsByTagName('base')[0].getAttribute('href');
                } else {
                    base = '';
                }

                window_width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
                $this = $(this);
                if (!$(this).hasClass('no-hover')) {
                    if (window_width < 768) {
                        if ($(e.target).attr('href') === undefined) {
                            if ($this.hasClass('toggle-ms')) {
                                slideHandler.call(this, e, $this);
                            } else {
                                if ($this.data('mobilehref') !== undefined) {
                                    window.manager.require('app:modules:ui:showLoader:show');
                                    window.location.href = base + $this.data('mobilehref');
                                } else if ($this.data('href') !== undefined) {
                                    window.manager.require('app:modules:ui:showLoader:show');
                                    window.location.href = base + $this.data('href');
                                } else {
                                    slideHandler.call(this, e, $this);
                                }
                            }
                        }
                    } else {
                        if ($(e.target).attr('href') === undefined) {
                            if (window_width <= 1100 && $this.hasClass('toggle-mt')) {
                                slideHandler.call(this, e, $this);
                            } else {
                                if ($this.data('href') !== undefined) {
                                    window.manager.require('app:modules:ui:showLoader:show');
                                    window.location.href = base + $this.data('href');
                                } else {
                                    slideHandler.call(this, e, $this);
                                }

                            }
                        }
                    }
                }
            });
        }
    });

    // delegacja pokazywania informacji o ostatnim poprawnym i niepoprawnym zalogowaniu
    window.manager.define('app:modules:ui:changeBackgrounds:run', {
        on: function () {
            $.each($('.offer-box, .offer-box-small'), function () {
                var $this = $(this),
                    bg1200 = $this.data('img-1200'),
                    bg768 = $this.data('img-768'),
                    bg320 = $this.data('img-320'),
                    setBg;

                if (bg768 === '') {
                    bg768 = bg1200;
                }

                if (bg320 === '') {
                    bg320 = bg1200;
                }

                setBg = function () {
                    var windowWidth = $(window).width();
                    if (windowWidth < 1100 && windowWidth >= 768) {
                        $this.css('background-image', "url('" + bg768 + "')");
                    } else if (windowWidth < 768) {
                        $this.css('background-image', "url('" + bg320 + "')");
                    } else {
                        $this.css('background-image', "url('" + bg1200 + "')");
                    }
                };

                setBg();
                $(window).on('resize', function () {
                    setBg();
                });
            });
        }
    });

    window.manager.define('app:modules:ui:showMoreDetails:run', {
        on: function () {
            $('.more-details-switch').on('click', function (e) {
                var $this;
                e.preventDefault();

                $this = $(this);

                $this.toggleClass('active');
                if ($this.hasClass('active')) {
                    $this.text('zwiń');
                } else {
                    $this.text('szczegóły karty');
                }
                $this.closest('.more-details-container').find('.more-details').slideToggle();
                $this.closest('.more-details-container').find('.more-details-btn').toggleClass('hide');
            });
        }
    });

    window.manager.define('app:modules:ui:showDialog:run', {
        on: function (type, title, content, action, to_remove, callback) {
            var html, id;

            id = 'dialog_' + new Date().getTime();

            if (type === 'confirm') {
                html = '<div id="' + id + '" title="' + title + '" class="hide">' +
                    '<p><i class="i-warning"></i> ' + content + '</p></div>';

                $('body').append(html);

                $('#' + id).dialog({
                    resizable: false,
                    draggable: false,
                    height: 160,
                    modal: true,
                    buttons: {
                        Delete: {
                            click: function () {
                                var $dialog = $(this);
                                $.post(action, function () {
                                    if (to_remove !== undefined && to_remove !== null) {
                                        to_remove.animate({
                                            opacity: 0
                                        }, function () {
                                            $(this).slideUp(function () {
                                                $(this).remove();
                                                callback.call(this);
                                            });
                                        });
                                    }
                                    $dialog.dialog("close");
                                });
                            },
                            'text': 'Tak',
                            'class': 'btn red small'
                        },
                        Cancel: {
                            click: function () {
                                var $dialog = $(this);
                                $dialog.dialog("close");
                            },
                            'text': 'Nie',
                            'class': 'btn gray small'
                        }
                    }
                });
            }
        }
    });

    window.manager.define('app:modules:ui:slideTo:run', {
        on: function () {
            var $trigger;

            $trigger = $('.slideto');

            $trigger.off('click').on('click', function (e) {
                var $target;

                e.preventDefault();

                $target = $($(this).attr('href'));

                $('html, body').animate({
                    scrollTop: $target.offset().top - 10
                }, 1000);
            });
        }
    });

    window.manager.define('app:modules:ui:tableRedirect:run', {
        on: function () {
            var $container;

            $container = $('.table-redirect');

            $('.table-header', $container).off('click').on('click', function (e) {
                var href, base;

                if (document.getElementsByTagName('base').length) {
                    base = document.getElementsByTagName('base')[0].getAttribute('href');
                } else {
                    base = '';
                }

                e.preventDefault();

                href = $(this).data('href');

                if (href !== undefined) {
                    window.manager.require('app:modules:ui:showLoader:show');
                    window.location.href = base + href;
                }
            });
        }
    });

    window.manager.define('app:modules:ui:onboarding:run', {
        on: function () {
            var closeUrl = $('#onboarding, #onboarding_2, #onboarding_3, #onboarding_4').eq(0).data('close-url'),
                prevUrl = $('#onboarding, #onboarding_2, #onboarding_3, #onboarding_4').eq(0).data('prev-url'),
                initUrl = $('#onboarding, #onboarding_2, #onboarding_3, #onboarding_4').eq(0).data('init-url'),
                initIndex = 0,
                initDelay = 0,
                initialWindowWidth,
                stepsOnboarding,
                scrollToTop = function () {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 300);
                },
//                disbaleTouchEvents = function () {
//                    var touchEventsTypes = 'click touchstart touchend MSGestureTap';
//                    $(document).off(touchEventsTypes, '#highlightedArea, #jpwOverlay, #overlayTop, #overlayRight, #overlayLeft, #overlayBottom')
//                        .on(touchEventsTypes, '#highlightedArea, #jpwOverlay, #overlayTop, #overlayRight, #overlayLeft, #overlayBottom', function (e) {
//                            scrollToTop();
//                        });
//                },
                hideMobileMenu = function () {
                    if ($('#nav_mobile').css('left') === '0px') {
                        $('#nav_mobile_trigger').trigger('click');
                    }
                },
                showMobileMenu = function () {
                    if ($('#nav_mobile').css('left') !== '0px') {
                        $('#nav_mobile_trigger').trigger('click');
                    }
                };

            initialWindowWidth = window.innerWidth || document.documentElement.clientWidth;

            //przeładowanie widoku po zmianie zakresu szerokości okna
            $(window).on('resize', function () {
                var width = window.innerWidth || document.documentElement.clientWidth;
                if (initialWindowWidth !== width) {
                    window.location = initUrl;
                }
            });

            if (initialWindowWidth >= 768) {
                if ($('#onboarding').length) {
                    stepsOnboarding = [
                        {
                            wrapper: '',
                            margin: '0',
                            popup: {
                                content: '#jpw_tooltip_1',
                                type: 'modal',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '500'
                            }
                        },
                        {
                            wrapper: '#jpw_step_2',
                            margin: '2px 5px 4px 1px',
                            popup: {
                                content: '#jpw_tooltip_2',
                                type: 'tooltip',
                                position: 'bottom',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '480'
                            }
                        },
                        {
                            wrapper: '#jpw_step_3',
                            margin: '2px 5px 4px 1px',
                            popup: {
                                content: '#jpw_tooltip_3',
                                type: 'tooltip',
                                position: 'bottom-left',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '500'
                            }
                        },
                        {
                            wrapper: '#jpw_step_4',
                            margin: '2px 4px 4px 2px',
                            popup: {
                                content: '#jpw_tooltip_4',
                                type: 'tooltip',
                                position: 'bottom-right',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '500'
                            }
                        },
                        {
                            wrapper: '#jpw_step_5',
                            margin: '0',
                            popup: {
                                content: '#jpw_tooltip_5',
                                type: 'tooltip',
                                position: 'bottom-left',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '500'
                            }
                        },
                        {
                            wrapper: '#jpw_step_6',
                            margin: '0',
                            popup: {
                                content: '#jpw_tooltip_6',
                                type: 'tooltip',
                                position: 'bottom-left',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '500'
                            }
                        },
                        {
                            wrapper: '#jpw_step_7',
                            margin: '0',
                            popup: {
                                content: '#jpw_tooltip_7',
                                type: 'tooltip',
                                position: 'bottom-left',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '500'
                            }
                        }
                    ];
                    $('#onboarding').pagewalkthrough({
                        steps: stepsOnboarding,
                        name: 'Walkthrough'
                    });
                } else if ($('#onboarding_2, #onboarding_3, #onboarding_4').length) {
                    window.location = initUrl;
                }
            } else {
                if ($('#onboarding').length) {
                    if (window.location.hash === '#back') {
                        initIndex = 4;
                        initDelay = 800;
                        showMobileMenu();
                    }
                    stepsOnboarding = [
                        {
                            wrapper: '',
                            margin: '0',
                            popup: {
                                content: '#jpw_tooltip_mobile_1',
                                type: 'modal',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '264'
                            }
                        },
                        {
                            wrapper: '#jpw_step_2',
                            margin: '2px 4px 4px 2px',
                            popup: {
                                content: '#jpw_tooltip_mobile_2',
                                type: 'tooltip',
                                position: 'bottom',
                                offsetHorizontal: -5,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                hideMobileMenu();
                                return true;
                            }
                        },
                        {
                            wrapper: '#nav_mobile_trigger',
                            margin: '0',
                            popup: {
                                content: '#jpw_tooltip_mobile_3',
                                type: 'tooltip',
                                position: 'bottom-left',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                showMobileMenu();
                                scrollToTop();
                                return true;
                            }
                        },
                        {
                            wrapper: '#jpw_step_4_mobile',
                            margin: '3px 4px 3px 2px',
                            popup: {
                                content: '#jpw_tooltip_mobile_4',
                                type: 'tooltip',
                                position: 'bottom-right',
                                offsetHorizontal: 15,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                showMobileMenu();
                                return true;
                            }
                        },
                        {
                            wrapper: '#empty',
                            margin: '0',
                            popup: {
                                content: '#empty',
                                type: 'tooltip',
                                position: 'bottom',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                window.location = $('#onboarding').data('mobile-url-next-step');
                            }
                        }
                    ];
                    $('#onboarding').pagewalkthrough({
                        steps: stepsOnboarding,
                        initStep: initIndex,
                        name: 'WalkthroughMobile'
                    });
                    if (window.location.hash === '#back') {
                        setTimeout(function () {
                            $.pagewalkthrough('prev');
                        }, initDelay);
                    }
                    window.location.hash = '';
//                } else if ($('#onboarding_2').length) {
//                    if (window.location.hash === '#back') {
//                        initIndex = 0;
//                    }
//                    stepsOnboarding = [
//                        {
//                            wrapper: '#jpw_step_6_mobile',
//                            margin: '0 -4px',
//                            popup: {
//                                content: '#jpw_tooltip_mobile_6',
//                                type: 'tooltip',
//                                position: 'bottom-right',
//                                offsetHorizontal: 19,
//                                offsetVertical: 0,
//                                width: '260'
//                            }
//                        },
//                        {
//                            wrapper: '#empty',
//                            margin: '0',
//                            popup: {
//                                content: '#empty',
//                                type: 'tooltip',
//                                position: 'bottom',
//                                offsetHorizontal: 0,
//                                offsetVertical: 0,
//                                width: '260'
//                            },
//                            onEnter: function () {
//                                window.location = $('#onboarding_2').data('mobile-url-next-step');
//                            }
//                        }
//                    ];
//                    setTimeout(function () {
//                        $('#onboarding_2').pagewalkthrough({
//                            steps: stepsOnboarding,
//                            initStep: initIndex,
//                            name: 'WalkthroughMobile'
//                        });
//                    }, initDelay);
//                    window.location.hash = '';
                } else if ($('#onboarding_3').length) {
                    if (window.location.hash === '#back') {
                        initIndex = 3;
                        initDelay = 800;
                        showMobileMenu();
                    }
                    stepsOnboarding = [
                        {
                            wrapper: '#jpw_step_7_mobile',
                            margin: '2px 2px 3px 2px',
                            popup: {
                                content: '#jpw_tooltip_mobile_7',
                                type: 'tooltip',
                                position: 'bottom',
                                offsetHorizontal: -10,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                hideMobileMenu();
                                return true;
                            }
                        },
                        {
                            wrapper: '#nav_mobile_trigger',
                            margin: '0',
                            popup: {
                                content: '#jpw_tooltip_mobile_8',
                                type: 'tooltip',
                                position: 'bottom-left',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                showMobileMenu();
                                scrollToTop();
                                return true;
                            }
                        },
                        {
                            wrapper: '#jpw_step_9_mobile',
                            margin: '3px 3px 3px 4px',
                            popup: {
                                content: '#jpw_tooltip_mobile_9',
                                type: 'tooltip',
                                position: 'bottom-left',
                                offsetHorizontal: 14,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                showMobileMenu();
                                return true;
                            }
                        },
                        {
                            wrapper: '#empty',
                            margin: '0',
                            popup: {
                                content: '#empty',
                                type: 'tooltip',
                                position: 'bottom',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                window.location = $('#onboarding_3').data('mobile-url-next-step');
                            }
                        }
                    ];
                    $('#onboarding_3').pagewalkthrough({
                        steps: stepsOnboarding,
                        initStep: initIndex,
                        name: 'WalkthroughMobile'
                    });
                    if (window.location.hash === '#back') {
                        setTimeout(function () {
                            $.pagewalkthrough('prev');
                        }, initDelay);
                    }
                    window.location.hash = '';
                } else if ($('#onboarding_4').length) {
                    stepsOnboarding = [
                        {
                            wrapper: '#jpw_step_10_mobile',
                            margin: '3px 3px 3px -3px',
                            popup: {
                                content: '#jpw_tooltip_mobile_10',
                                type: 'tooltip',
                                position: 'bottom-right',
                                offsetHorizontal: 19,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                hideMobileMenu();
                                return true;
                            }
                        },
                        {
                            wrapper: '#nav_mobile_trigger',
                            margin: '0',
                            popup: {
                                content: '#jpw_tooltip_mobile_11',
                                type: 'tooltip',
                                position: 'bottom-left',
                                offsetHorizontal: 20,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                showMobileMenu();
                                scrollToTop();
                                return true;
                            }
                        },
                        {
                            wrapper: '#jpw_step_13_mobile',
                            margin: '3px',
                            popup: {
                                content: '#jpw_tooltip_mobile_12',
                                type: 'tooltip',
                                position: 'bottom-left',
                                offsetHorizontal: 19,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                showMobileMenu();
                                return true;
                            }
                        },
                        {
                            wrapper: '#empty',
                            margin: '0',
                            popup: {
                                content: '#empty',
                                type: 'tooltip',
                                position: 'bottom',
                                offsetHorizontal: 0,
                                offsetVertical: 0,
                                width: '260'
                            },
                            onEnter: function () {
                                window.location = $('#onboarding_4').data('mobile-url-next-step');
                            }
                        }
                    ];
                    $('#onboarding_4').pagewalkthrough({
                        steps: stepsOnboarding,
                        name: 'WalkthroughMobile'
                    });
                    window.location.hash = '';
                }
            }

            //przejście do kolejnego kroku
            $(document).off('click', '.prev-step').on('click', '.prev-step', function (e) {
                e.preventDefault();
                $.pagewalkthrough('prev', e);
            });

            //przejście do następnego kroku
            $(document).off('click', '.next-step').on('click', '.next-step', function (e) {
                e.preventDefault();
                $.pagewalkthrough('next', e);
            });

            //otwarcie popupu z opcjami "nie pokazuj wiecej" i "zamknij"
            $(document).off('click', '.close-step').on('click', '.close-step', function (e) {
                e.preventDefault();
                $.pagewalkthrough('close');
                if (!$(this).hasClass('is-last')) {
                    var oShadowBox = new ShadowBox({
                        classes: 'popup popup-red popup-red-wide',
                        content: '<div class="grid text-white">' +
                            //                        '<p>zamknij</p>' +
                            '<div class="padding-10-0"><a href="#" class="btn gray float-left disable-onboarding">nie pokazuj więcej</a></div>' +
                            '<div class="c"></div>' +
                            '<div class="padding-10-0"><a href="#" class="btn gray float-left arrow close-onboarding">zamknij</a></div>' +
                            '</div>'
                    });
                    return oShadowBox;
                }
            });

            //wyłączenie onboardingu
            $(document).off('click', '.disable-onboarding').on('click', '.disable-onboarding', function (e) {
                e.preventDefault();
                $.post(closeUrl, {
                    disableOnboarding: true
                }).success(function () {
                    window.location = closeUrl;
                });
            });

            //zamkniecie onboardingu
            $(document).off('click', '.close-onboarding').on('click', '.close-onboarding', function (e) {
                e.preventDefault();
                window.location = closeUrl;
            });

            //powrót do kroku z poprzedniego widoku
            $(document).off('click', '.prev-view').on('click', '.prev-view', function (e) {
                e.preventDefault();
                window.location = prevUrl + '#back';
            });
        }
    });
}());
/*global window, $, console, document, setTimeout, alert, isNaN */

(function () {
    'use strict';
    window.manager.define('app:modules:accounts:dashboardItem:run', {
        on: function () {
            var $container, showWidgets, slideHandler;
            showWidgets = function ($target, $widgets_visible) {
                $target.siblings('.accounts-widgets').stop(true, false).slideDown(500);
                $container.find('.table-details:visible').slideUp(500);
                $('html, body').animate({
                    scrollTop: $('#accounts_list').offset().top - 80
                }, 500);
            };

            slideHandler = function (event, toggler) {
                event.preventDefault();

                var $details_visible = $('.table-details:visible', $container);

                if (!toggler.children('.table-details').is(':visible')) {
                    if ($details_visible.length) {
                        $details_visible.stop(true, false).slideUp(500);
                        toggler.children('.table-details').slideDown(500);
                    } else {
                        toggler.children('.table-details').slideDown(500);
                    }
                } else {
                    $details_visible.stop(true, false).slideUp(500);
                    toggler.children('.table-details').slideUp(500);
                    toggler.removeClass('active');
                }
            };

            $container = $('#accounts_list');
            $('.account-details', $container).off('click').on('click', function (e) {
                var $this, $widgets_visible, window_width;

                window_width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
                $this = $(this);

                if (window_width < 768) {
                    $('.account-details.active', $container).removeClass('active');
                    $this.addClass('active');
                    slideHandler.call(this, e, $this);
                } else {
                    if ($(e.target).attr('href') === undefined) {
                        e.preventDefault();

                        $widgets_visible = $('.accounts-widgets:visible', $container);

                        if (!$this.siblings('.accounts-widgets').is(':visible')) {
                            $widgets_visible.siblings('.account-details.active').removeClass('active');
                            if ($widgets_visible.length) {
                                $widgets_visible.stop(true, false).slideUp(500);
                                showWidgets.call(this, $this, $widgets_visible);
                            } else {
                                $this.addClass('active');
                                showWidgets.call(this, $this, $widgets_visible);
                            }
                        } else {
                            $this.addClass('active');
                            slideHandler.call(this, e, $this);
                        }
                    }
                }
            });
        }
    });
    window.manager.define('app:modules:accounts:dashboardItemAsLink:run', {
        on: function () {
            $('.account-details[data-href]').off('click').on('click', function (e) {
                var $this, base;

                if (document.getElementsByTagName('base').length) {
                    base = document.getElementsByTagName('base')[0].getAttribute('href');
                } else {
                    base = '';
                }
                $this = $(this);

                window.manager.require('app:modules:ui:showLoader:show');
                window.location.href = base + $this.data('href');
            });
        }
    });

    window.manager.define('app:modules:accounts:sendSelect:run', {
        on: function () {
            var container = $('#send_select'),
                url = container.data('url'),
                data = {
                    value: container.find('option:selected').val()
                };

            container.on('change', function (e) {
                container.removeClass('has-error is-valid').find('.error').remove();
                $.post(url, data)
                    .fail(function () {
                        container.addClass('has-error').append('<span class="error">wystąpił błąd przy zapisie - spróbuj ponownie</span>');
                    }).success(function () {
                        container.addClass('is-valid');
                    });
            });
        }
    });

    window.manager.define('app:modules:accounts:widgetHistory:rowClick:run', {
        on: function () {
            var $container, $trigger;

            $container = $('#accounts_list');
            $trigger = $('.table-transfers td', $container);

            $trigger.off('click').on('click', function (e) {
                var $this, $row, base;

                if (document.getElementsByTagName('base').length) {
                    base = document.getElementsByTagName('base')[0].getAttribute('href');
                } else {
                    base = '';
                }
                $this = $(this);
                $row = $this.parentsUntil('.table-transfers', 'tr');

                if ($row.data('href') !== undefined) {
                    window.manager.require('app:modules:ui:showLoader:show');
                    window.location.href = base + $row.data('href');
                }
            });
        }
    });

//    window.manager.define('app:modules:accounts:linkToDetails:run', {
//        on: function () {
//            var $container = $('#accounts');
//            $('.account-details', $container).off('click').on('click', function (e) {
//                e.preventDefault();
//                var url = $(this).data('mobilehref');
//
//                if ($(e.target).hasClass('btn')) {
//                    url = $(e.target).attr('href');
//                }
//                if (url !== undefined) {
//                    window.location.href = url;
//                }
//                return false;
//            });
//        }
//    });

    window.manager.define('app:modules:accounts:history:transactionsResults:run', {
        on: function () {

            var container = $('#history_timeline, #history_list'),
                outgo = function () {
                    var outgoes = container.find('.history-timeline-container[data-outgo], .history-list-item[data-outgo]'),
                        outgoesLength = outgoes.length,
                        sum = 0,
                        decimal = 0,
                        i = 0;

                    for (i; i < outgoesLength; i = i + 1) {
                        sum = sum + parseInt(outgoes.eq(i).data('outgo'), 10);
                    }

                    $('#history_transactions_sum').data('outgoes-sum', sum);

                    sum = sum.toString();
                    decimal = decimal.toString();

                    if (sum.toString().length >= 3) {
                        sum = sum.toString();
                        decimal = sum.substring(sum.length - 2, sum.length);
                        sum = '-' + sum.substring(0, sum.length - 2);
                    } else if (sum.toString().length === 2) {
                        decimal = sum.toString();
                        sum = '-0';
                    } else if (sum.toString().length === 1) {
                        decimal = '0' + sum.toString();
                        sum = sum.toString();
                        sum = '-0';
                    } else {
                        sum = '-';
                        decimal = '--';
                    }

                    $('#history_transactions_outgo').text(sum.replace(/(?=(\d{3})+(?!\d))/g, " "));
                    $('#history_transactions_outgo_decimal').text(',' + decimal);
                },
                income = function () {
                    var incomes = container.find('.history-timeline-container[data-income], .history-list-item[data-income]'),
                        incomesLength = incomes.length,
                        sum = 0,
                        decimal = 0,
                        i = 0,
                        m = 0,
                        finalSum = '',
                        j,
                        k;

                    for (i; i < incomesLength; i = i + 1) {
                        sum = sum + parseInt(incomes.eq(i).data('income'), 10);
                    }

                    sum = sum.toString();
                    decimal = decimal.toString();

                    $('#history_transactions_sum').data('incomes-sum', sum);

                    if (sum.toString().length >= 3) {
                        sum = sum.toString();
                        decimal = sum.substring(sum.length - 2, sum.length);
                        sum = sum.substring(0, sum.length - 2);
                    } else if (sum.toString().length === 2) {
                        decimal = sum.toString();
                        sum = '0';
                    } else if (sum.toString().length === 1) {
                        decimal = '0' + sum.toString();
                        sum = '0';
                    } else {
                        sum = '-';
                        decimal = '--';
                    }
                    j = sum.length;
                    if (j >= 4) {
                        for (m; m < j; m++) {
                            k = j % 3;
                            if (k === m % 3) {
                                finalSum = finalSum + ' ' + sum[m];
                            } else {
                                finalSum = finalSum + sum[m];
                            }
                        }
                        sum = finalSum;
                    }

                    $('#history_transactions_income').text(sum);
                    $('#history_transactions_income_decimal').text(',' + decimal);
                },
                results = function () {
                    $('#history_transactions_lenght').text(container.find('.history-timeline-container[data-income], .history-timeline-container[data-outgo], .history-list-item[data-income], .history-list-item[data-outgo]').length);
                },
                summary = function () {
                    var summaryContainer = $('#history_transactions_sum'),
                        summaryContainerDecimal = $('#history_transactions_sum_decimal'),
                        summaryIncomes = summaryContainer.data('incomes-sum'),
                        summaryOutgoes = summaryContainer.data('outgoes-sum'),
                        sum,
                        decimal = 0,
                        i = 1,
                        j,
                        k,
                        finalSum = '';

                    sum = (summaryIncomes - summaryOutgoes);

                    sum = sum.toString();
                    decimal = decimal.toString();

                    if (sum >= 0) {
                        summaryContainer.parent().removeClass('amount-minus');
                        if (sum.toString().length >= 3) {
                            sum = sum.toString();
                            decimal = sum.substring(sum.length - 2, sum.length);
                            sum = sum.substring(0, sum.length - 2);
                        } else if (sum.toString().length === 2) {
                            decimal = sum.toString();
                            sum = '0';
                        } else if (sum.toString().length === 1) {
                            decimal = '0' + sum.toString();
                            sum = sum.toString();
                        } else {
                            sum = '-';
                            decimal = '--';
                        }
                        j = sum.length;
                        if (j >= 4) {
                            i = 0;
                            for (i; i < j; i++) {
                                k = j % 3;
                                if (k === i % 3) {
                                    finalSum = finalSum + ' ' + sum[i];
                                } else {
                                    finalSum = finalSum + sum[i];
                                }
                            }
                            sum = finalSum;
                        }
                    } else {
                        summaryContainer.parent().addClass('amount-minus');
                        if (sum.toString().length >= 4) {
                            sum = sum.toString();
                            decimal = sum.substring(sum.length - 2, sum.length);
                            sum = sum.substring(0, sum.length - 2);
                        } else if (sum.toString().length === 3) {
                            sum = sum.toString();
                            decimal = sum.substring(sum.length - 2, sum.length);
                            sum = '-0';
                        } else if (sum.toString().length === 2) {
                            decimal = '0' + sum.toString().substring(sum.length - 1, sum.length);
                            sum = '-0';
                        } else {
                            sum = '-0';
                            decimal = '--';
                        }
                        sum = sum.substring(1, sum.length);
                        j = sum.length;
                        i = 0;
                        for (i; i < j; i++) {
                            k = j % 3;
                            if (k === i % 3) {
                                finalSum = finalSum + ' ' + sum[i];
                            } else {
                                finalSum = finalSum + sum[i];
                            }
                        }
                        sum = '-' + finalSum;
                    }
                    summaryContainer.text(sum);
                    summaryContainerDecimal.text(',' + decimal);
                };

            outgo();
            income();
            results();
            summary();
        }
    });

    window.manager.define('app:modules:accounts:history:filters:run', {
        on: function () {
//            var $filtersForm = $('#history_account_filters'),
            var rangeType = $('#history_range_select option:selected').val(),
                dateRangeStart = $('#history_range_start').val(),
                dateRangeEnd = $('#history_range_end').val(),
                transactionType = $('#filter_transaction_types option:selected').val();

            if ($('#filter_savings_select').length) {
                window.manager.require('app:modules:accounts:history:savingsSelect:run');
            }

            //filtry - wysyłanie zapytania i wyświetlanie wyników
//            $filtersForm.on('submit', function (e) {
//                e.preventDefault();
//                var url = $filtersForm.attr('action'),
//                    data = $filtersForm.serialize(),
//                    container = $('#history_timeline, #history_list'),
//                    placer = $('#history_transaction_placer');

//                if (!$filtersForm.find('.has-error').length && !$filtersForm.find('.range-error').length) {
//                    e.preventDefault();
//                    $filtersForm.addClass('disabled');
//                    $.post(url, data, function (res) {
//                        container.children('.history-timeline-box, .history-timeline-container, .history-list-item').fadeOut(200).remove();
//                        placer.before(res);
//                    }).always(function () {
//                        $filtersForm.removeClass('disabled');
//                        $('.category-select').not('[data-category-select="active"]').find('select').each(function () {
//                            var $this = $(this);
//                            if ($this.data('uniformed') === undefined && !$this.hasClass('nouniform')) {
//                                $this.uniform({
//                                    selectAutoWidth: false,
//                                    wrapperClass: $this.data('classes') || null
//                                });
//                            }
//                        });
//
//                        $('.category-select').not('[data-category-select="active"]').categorySelect(window.categories, true);
//
//                        // animacja doładowanych transakcji
//                        setTimeout(function () {
//                            $('.history-timeline-container, .history-timeline-box, .history-list-item', '#history_account').not(':visible').fadeIn(600);
//                        }, 400);
//
//                        window.manager.require('app:modules:accounts:history:transactionsResults:run');
//                        window.manager.require('app:modules:accounts:history:timeline:run');
//                    });
//                }
//            });

//            $filtersForm.find('.search-submit').on('click', function (e) {
//                e.preventDefault();
//                $filtersForm.trigger('submit');
//            });


            $('.history-filter-submit').on('click', function (e) {
                e.preventDefault();
                setTimeout(function () {
                    if (!$('#history_account_filters').find('.has-error, .range-error').length) {
                        $('#history_account_filters').trigger('submit');
                    }
                }, 200);
            });

            $('#history_range_select').on('change', function () {
                $('#range_type1').prop('checked', true);
                $.uniform.update();
            });

            $('#history_range_start, #history_range_end').on('change', function () {
                $('#range_type2').prop('checked', true);
                $.uniform.update();
            });

            //czyszczenie wartości w filtrach zaawansowanych
            $('#history_filter_advanced_reset').on('click', function (e) {
                e.preventDefault();
                $('#filter_amount_min, #filter_amount_max').val('').trigger('focus').trigger('blur').val('').closest('.field').removeClass('has-error is-valid');
                $('#filter_search_text').val('').trigger('focus').trigger('blur');
                $('#history_range_select option[value="' + rangeType + '"]').prop('selected', true);
                $('#history_range_start').val(dateRangeStart);
                $('#history_range_end').val(dateRangeEnd);
                $('#range_type1').trigger('click');
                $('#filter_type').val('');
                $('#filter_transaction_types option[value="' + transactionType + '"]').prop('selected', true);
                $('#savings_capitalisation').prop('checked', false);
                $('.category-select-primary', '#filter_category_select').val($('.category-select-primary', '#filter_category_select option:first').val()).trigger('change');
				$('#form_odseteki').prop('checked', true);
                $.uniform.update();
//                $filtersForm.trigger('submit');
            });

            //inicjalizacja datepickera dla daty początkowej zakresu historii transakcji
            $('#history_range_start').datepicker({
                dateFormat: window.ui_config.dpDateFormat,
                monthNames: window.ui_config.dpMonthNames,
                dayNamesMin: window.ui_config.dpDayNamesMin,
                maxDate: 0,
                onClose: function (selectedDate) {
                    $("#history_range_end").datepicker("option", "minDate", selectedDate);
                }
            });

            //inicjalizacja datepickera dla daty końcowej zakresu historii transakcji
            $('#history_range_end').datepicker({
                dateFormat: window.ui_config.dpDateFormat,
                monthNames: window.ui_config.dpMonthNames,
                dayNamesMin: window.ui_config.dpDayNamesMin,
                maxDate: 0,
                onClose: function (selectedDate) {
                    $("#history_range_start").datepicker("option", "maxDate", selectedDate);
                }
            });

            // otwieranie datepickerów po kliknięciu w ikonę kalendarza
            $('.history-account-settings .i-calendar').on('click', function () {
                $(this).siblings('.history-range-input').trigger('focus');
            });

            //rozwijanie filtrów zaawansowanych w  historii konta
            $('#history_filter_switch').on('click', function (e) {
                e.preventDefault();
                $('#history_filter_advanced').stop().slideToggle(600);
                $(this).find('.raq-icon').toggleClass('raquo-down raquo-up');
            });

            //rozwijanie filtrów zaawansowanych w  historii konta w mobile smartphone
            $('#history_filter_switch_ms').off('click').on('click', function (e) {
                e.preventDefault();
                $('#history_filter_advanced').add('.history-row-advanced-ms', '#history_filter').stop().slideToggle(400);
                $(this).find('.raq-icon').toggleClass('raquo-down raquo-up');
            });

            // filtr kwoty od/do
            $('#filter_amount_min, #filter_amount_max').on('blur', function (e) {
                var filterMin = $('#filter_amount_min'),
                    filterMax = $('#filter_amount_max'),
                    amountMin = parseFloat(filterMin.val().replace(',', '.'), 10).toFixed(2),
                    amountMax = parseFloat(filterMax.val().replace(',', '.'), 10).toFixed(2);

                if (isNaN(amountMin)) {
                    amountMin = 0;
                }

//                $('#filter_amount_min').val(filterMin.val().replace('.', ','));
//                $('#filter_amount_max').val(filterMax.val().replace('.', ','));

                $('#history_account_filters').addClass('disabled').removeClass('active');
                filterMin.closest('.field').removeClass('range-error');
                filterMax.closest('.field').removeClass('range-error').find('.filter-range-error').remove();

                if (Number(amountMin) > Number(amountMax)) {
                    filterMin.closest('.field').addClass('range-error').removeClass('is-valid');
                    filterMax.closest('.field').addClass('range-error').removeClass('is-valid').append('<span class="filter-range-error ms-text-right">nieprawidłowy zakres kwot</span>');
                } else {
                    $('#history_account_filters').removeClass('disabled').addClass('active');
                }
            });

            // reset widoczności elementów filtra historii konta w zależności od przedziału szerokości okna w RWD
            $(window).on('resize', function () {
                var windowWidth = $(window).width(),
                    $form = $('#history_account_filters');

                if (!($.browser.msie && $.browser.version < 9)) {
                    if ($form.hasClass('filters-ms')) {
                        if (windowWidth >= 768) {
                            $form.find('.history-row-advanced-ms, .history-account-settings-advanced').removeAttr('style');
                            $('.raquo-up', '#history_filter_switch_ms, #history_filter_switch').addClass('raquo-down').removeClass('raquo-up');
                        }
                    } else {
                        if (768 < windowWidth) {
                            $form.find('.history-row-advanced-ms, .history-account-settings-advanced').removeAttr('style');
                            $('.raquo-up', '#history_filter_switch_ms, #history_filter_switch').addClass('raquo-down').removeClass('raquo-up');
                        }
                    }

                    if (768 <= windowWidth) {
                        $form.removeClass('filters-ms');
                    } else {
                        $form.addClass('filters-ms');
                    }
                }
            });

            $(window).on('load', function () {
                var windowWidth = $(window).width(),
                    $form = $('#history_account_filters');

                if (768 < windowWidth) {
                    $form.removeClass('filters-ms');
                } else {
                    $form.addClass('filters-ms');
                }
            });

           
            // rozwijanie szczegółów transakcji na osi czasu w historii konta, kart debetowych,kart kredytowych
            $(document).on('click', '.history-timeline-item-header', function (e) {
                e.preventDefault();
                var windowWidth = $(window).width();
                var $this;
                $this = $(this);
                
              	var szczegoly_os_smartfon =$this.parents().nextAll('.history-item-details').first();
                var szczegoly_os =$this.nextAll('.history-item-details').first();
                if(windowWidth<768){
                	if(szczegoly_os.is(':visible')){
                    szczegoly_os.stop().slideToggle();
                    }
                	szczegoly_os_smartfon.stop().slideToggle();
                }else{
                	if(szczegoly_os_smartfon.is(':visible')){
                	szczegoly_os_smartfon.stop().slideToggle();
                	}
                    szczegoly_os.stop().slideToggle();
                }
                
                if ($this.hasClass('active')) {
                    $this.removeClass('active');
                } else {
                    $this.addClass('active');
                }
            });
            // rozwijanie szczegółów transakcji na liscie w historii konta, kart debetowych,kart kredytowych
            $(document).on('click', '.history-list-item-head', function (e) {
                e.preventDefault();
                var $this;
                $this = $(this);
                var szczegoly =$this.nextAll('.history-list-item-details').first();
                szczegoly.stop().slideToggle();

                if ($this.hasClass('active')) {
                    $this.removeClass('active');
                } else {
                    $this.addClass('active');
                }
            });
        }
    });


    window.manager.define('app:modules:accounts:history:calendar:run', {
        on: function () {
            // wywołanie kalendarza
            var $historyCalendar = $('#history_calendar'),
//                zIndex = 10,
                //obsługa tooltipów
//                eventTooltip = function (el) {
//                    var $el = $(el),
//                        initialZIndex;
//
//                    $el.on('click', function (e) {
//                        if ($(window).width() <= 1100 && !$(e.target).hasClass('calendar-tooltip-link')) {
//                            e.preventDefault();
//                        }
//                    });
//
//
//                    $el.on('mouseenter', function (e) {
//                        if ($(window).width() > 1100) {
//                            initialZIndex = $(this).closest('.fc-event').css('z-index');
//                            zIndex++;
//                            $(this).find('.calendar-tooltip').stop().fadeIn(200).closest('.fc-event').css('z-index', zIndex);
//                        }
//                    });
//                    $el.on('mouseleave', function () {
//                        if ($(window).width() > 1100) {
//                            $(this).find('.calendar-tooltip').stop().fadeOut(200).closest('.fc-event').css('z-index', initialZIndex);
//                        }
//                    });
//
//                    $el.on('click touchend', function (e) {
//                        var $that = $(this),
//                            $evBox = $that.closest('.fc-event'),
//                            $otherBtn = $historyCalendar.find('.fc-event').not($evBox);
//
//                        zIndex++;
//                        if ($(window).width() <= 1100 && $(window).width() >= 768 && !$that.hasClass('calendar-event-more')) {
//                            initialZIndex = $(this).closest('.fc-event').css('z-index');
//                            $(this).find('.calendar-tooltip').stop().fadeIn(200);
//                            $evBox.css('zIndex', zIndex);
//                            $otherBtn.css('zIndex', 10).find('.calendar-event-more').html('więcej <span class="highlight">&raquo;</span>').removeClass('active').siblings('.calendar-event.hide').slideUp();
//                        }
//                        $(document).on('click touchend', function (e) {
//                            if ($(window).width() <= 1100 && $(window).width() >= 768 && $(e.target).closest($that).length === 0) {
//                                $that.find('.calendar-tooltip').stop().fadeOut(200);
//                            }
//                        });
//                    });
//                },
                eventPopup = function (element, event) {
                    var $el = $(element),
                        ev = event,
                        withLeadZeroes;

                    withLeadZeroes = function (date) {
                        if (date < 10) {
                            return '0' + date;
                        }
                        return date;
                    };

                    $el.off('click').on('click', function () {
                        var el,
                            i = 0,
                            j = ev.transactions.length,
                            evPopup = '',
//                            linkTo = '',
//                            hasDesc = '',
//                            elDescription = '',
                            isNegative = '',
//                                $this = $(this),
                            oShadowbox = new window.ShadowBox({
                                classes: 'calendar-event-shadowbox',
                                content: '<div id="calendar_event_popup" class="calendar-event-popup-container"></div>',
                                callback: function () {
                                    evPopup = evPopup +
                                        "<h2>" + withLeadZeroes(ev.start.getDate()) + '.' + withLeadZeroes(ev.start.getMonth() + 1) +
                                        '.' + withLeadZeroes(ev.start.getFullYear()) + "</h2>";

                                    for (i = 0; i < j; i++) {
                                        el = ev.transactions[i];
                                        evPopup = evPopup + '<div class="table-alike">';
//                                        if (el.hasOwnProperty('url') && el.url !== '') {
//                                            linkTo = ' href="' + el.url + '" ';
//                                        } else {
//                                            linkTo = '';
//                                        }

//                                        if (el.hasOwnProperty('description') && el.description !== '') {
//                                            hasDesc = 'calendar-tooltip';
//                                            elDescription = '<div class="calendar-tooltip hide"><span class="calendar-tooltip-content ' + hasDesc + '">' + el.description + '<br><a class="calendar-tooltip-link" ' + linkTo + '>więcej <span class="highlight">&raquo;</span></a></span></div>';
//                                        } else {
//                                            hasDesc = '';
//                                            elDescription = '';
//                                        }

                                        if (el.hasOwnProperty('textMain') && el.textMain !== '' && el.textMain[0] === "-") {
                                            isNegative = 'minus';
                                        } else {
                                            isNegative = '';
                                        }

                                        if (i === 0) {
                                            evPopup = evPopup +
                                                '<div class="grid calendar-event-popup-row table-header active">';
                                        } else {
                                            evPopup = evPopup +
                                                '<div class="grid calendar-event-popup-row table-header">';
                                        }

                                        // pokazywanie ikony
                                        evPopup = evPopup +
                                            '<div class="table-grid-4 table-grid-ms-7">' +
                                                '<i class="i-transaction-' + el.iconType + '"></i>' +
                                            '</div>';

                                        // pokazywanie rodzaju transakcji
                                        evPopup = evPopup +
                                            '<div class="table-grid-23 table-grid-ms-21">' +
                                                '<span class="desc">' + el.textType + '</span>' +
                                            '</div>';

                                        if (el.hasOwnProperty('textMain') && el.textMain !== '') {
                                            evPopup = evPopup +
                                                '<div class="table-grid-21 table-grid-ms-20">' +
                                                    '<span class="fancy-amount right ' + isNegative + '">' +
                                                    '<span class="value-wrapper">' + el.textMain + '</span>' +
                                                    '<span class="decimal-wrapper">,' + el.textDecimal + '</span>' +
                                                    '<span class="currency-wrapper">PLN</span>' +
                                                    '</span>' +
                                                '</div>';
                                        } else {
                                            evPopup = evPopup + '<div class="table-grid-38">&nbsp;</div>';
                                        }

                                        // zamkniecie .table-header
                                        evPopup = evPopup + "</div>";

                                        // ukryte szczegoly transakcji
                                        if (i === 0) {
                                            evPopup = evPopup +
                                                '<div class="table-details desc">';
                                        } else {
                                            evPopup = evPopup +
                                                '<div class="table-details desc hide">';
                                        }

                                        if (el.nrb !== undefined && el.nrb !== '') {
                                            evPopup = evPopup +
                                                '<div class="row">' +
                                                '<div class="grid-12 grid-ms-16">rachunek obciążony:</div>' +
                                                '<div class="grid-36 grid-ms-32">' +
                                                el.nrb +
                                                '</div>' +
                                                '</div>';
                                        }
                                        if (el.receiver !== undefined && el.receiver !== '') {
                                            evPopup = evPopup +
                                                '<div class="row">' +
                                                '<div class="grid-12 grid-ms-16">odbiorca:</div>' +
                                                '<div class="grid-36 grid-ms-32">' +
                                                el.receiver +
                                                '</div>' +
                                                '</div>';
                                        }
                                        if (el.title !== undefined && el.title !== '') {
                                            evPopup = evPopup +
                                                '<div class="row">' +
                                                '<div class="grid-12 grid-ms-16">tytuł:</div>' +
                                                '<div class="grid-36 grid-ms-32">' +
                                                el.title +
                                                '</div>' +
                                                '</div>';
                                        }
                                        
                                        
                                        	
                                    	
                                        evPopup = evPopup +
                                        '<div class="row">' +
                                        '<div class="grid-12 grid-ms-16">kategoria:</div>' +
                                        '<div class="grid-36 grid-ms-32 category-select">' +
                                        '<select class="category-select-primary" data-classes="selector-small selector-mt-small">' +
                                        	'<option value="">wybierz kategorię</option>' +
                                        '</select>' +
                                        '<select class="category-select-secondary" data-classes="selector-small selector-mt-small" >' +
                                        	'<option value="">wybierz podkategorię</option>' +
                                        '</select>' +
                                        '</div>' +
                                        '</div>';
                                        

                                        // zamknięcie .table-details
                                        evPopup = evPopup + "</div>";

                                        // zamkniecie .table-alike
                                        evPopup = evPopup + "</div>";
                                    }
                                    $('#calendar_event_popup').html(evPopup);
                                    
                                    $('#calendar_event_popup').find('.category-select').categorySelect(window.categories, true);

                                    $('.calendar-event-popup-row').off('click').on('click', function (e) {
                                        var $visible = $('.table-details:visible', $('#calendar_event_popup')),
                                            $that = $(this);

                                        if ($visible.length) {
                                            $visible.stop().slideUp(200);
                                            $visible.siblings('.table-header').removeClass('active');
                                        }

                                        if ($that.siblings('.table-details').is(':visible')) {
                                            $that.siblings('.table-details').stop().slideUp(200);
                                            $that.removeClass('active');
                                        } else {
                                            $that.siblings('.table-details').stop().slideDown(200);
                                            $that.addClass('active');
                                        }
//                                        $(document).one('click touchend', function (e) {
//                                            if ($(e.target).closest($that).length === 0) {
//                                                $that.find('.calendar-tooltip').stop().fadeOut(200);
//                                            }
//                                        });
                                    });
                                }
                            });
                        return oShadowbox;
                    });
                };

            $historyCalendar.fullCalendar({
                monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
                monthNamesShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
                dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'],
                dayNamesShort: ['ND', 'PN', 'WT', 'ŚR', 'CZ', 'PT', 'SB'],
                firstDay: 1,
                events: 'kalendarz.json',
                header: false,
                eventRender: function (event, element) {
                    var eventData,
                        transactions = event.transactions,
                        i,
                        j = transactions.length,
                        el,
                        isNegative,
                        isPast = '';
//                        linkTo = '',
//                        hasDesc,
//                        elDescription,
//                        mobileLink;

                    if (event.hasOwnProperty('past') && event.past === 'true') {
                        isPast = 'calendar-events-wrapper-past';
                    } else {
                        isPast = '';
                    }

                    eventData = '<div class="calendar-events-container">';
                    eventData += '<div class="calendar-events-wrapper ' + isPast + '">';

                    for (i = 0; i < j; i = i + 1) {
                        el = transactions[i];
                        if (el.type === 'transfer') {
//                            if (el.hasOwnProperty('url') && el.url !== '') {
//                                linkTo = ' href="' + el.url + '" ';
//                                mobileLink = '<a href="' + el.url + '" class="calendar-tooltip-link">więcej <span class="highlight">&raquo;</span></a>';
//                            } else {
//                                linkTo = '';
//                                mobileLink = '';
//                            }

//                            if (el.hasOwnProperty('description') && el.description !== '') {
//                                hasDesc = 'calendar-tooltip';
//                                elDescription = '<span class="calendar-tooltip hide"><span class="calendar-tooltip-content">' + '<i class="calendar-icon icon-calendar-' + el.iconTypeTablet + '"></i>' + el.description + ' ' + mobileLink + '</span></span>';
//                            } else {
//                                hasDesc = '';
//                                elDescription = '';
//                            }

                            if (i < 2) {
                                eventData += '<div class="calendar-event">';
                            } else {
                                eventData += '<div class="calendar-event hide">';
                            }

                            if (el.textMain[0] === "-") {
                                isNegative = 'minus';
                            } else {
                                isNegative = '';
                            }

                            eventData = eventData +
                                '<i class="calendar-icon i-transaction-' + el.iconType + '"></i>' +
                                '<span class="fancy-amount ' + isNegative + ' right">' +
                                    '<span class="value-wrapper">' + el.textMain + '</span>' +
                                    '<span class="decimal-wrapper">,' + el.textDecimal + '</span>' +
                                    '<span class="currency-wrapper">PLN</span>' +
                                '</span>' +
                                '</div>';
                        } else if (el.type === 'message') {
//                            if (el.hasOwnProperty('url') && el.url !== '') {
//                                linkTo = ' href="' + el.url + '" ';
//                                mobileLink = '<a href="' + el.url + '" class="calendar-tooltip-link">więcej <span class="highlight">&raquo;</span></a>';
//                            } else {
//                                linkTo = '';
//                                mobileLink = '';
//                            }

//                            if (el.hasOwnProperty('description') && el.description !== '') {
//                                hasDesc = 'calendar-tooltip';
//                                elDescription = '<span class="calendar-tooltip hide"><span class="calendar-tooltip-content">' + '<i class="calendar-icon icon-calendar-' + el.iconTypeTablet + '"></i>' + el.description + ' ' + mobileLink + '</span></span>';
//                            } else {
//                                hasDesc = '';
//                                elDescription = '';
//                            }

                            if (i < 2) {
                                eventData += '<div class="calendar-event">';
                            } else {
                                eventData += '<div class="calendar-event hide">';
                            }

                            eventData = eventData +
                                '<span>' +
                                    '<i class="calendar-icon i-transaction-' + el.iconType + ' mt-show-calendar-icon right"></i>' +
                                '</span>' +
                                '</div>';
                        }
                    }

                    if (j > 2) {
                        eventData += '<div class="calendar-event calendar-event-more text-right">więcej <span class="highlight">&raquo;</span></div>';
                    }

                    //popup smartphone
                    eventData += '<div class="calendar-events-popup-trigger active ms-show"><i class="i-nav-payments right"></i></div>';

                    eventData += '</div>';
                    eventData += '</div>';
                    element.find('.fc-event-title').html(eventData);

                    //uzupełnienie podsumowania na podstawie obiektu z właściwości summary ostatniego eventu w JSONie
                    if (transactions[j - 1].hasOwnProperty('summary')) {

                        //łączna liczba operacji
                        $('#calendar_transactions_lenght').text(transactions[j - 1].summary.calendarTransactionsLength);

                        //zaplanowane wpływy
                        $('#calendar_transactions_income').text(transactions[j - 1].summary.calendarIncome);
                        $('#calendar_transactions_income_decimal').text(transactions[j - 1].summary.calendarIncomeDecimal);

                        //zaplanowane wydatki
                        $('#calendar_transactions_outgo').text(transactions[j - 1].summary.calendarOutgo);
                        $('#calendar_transactions_outgo_decimal').text(transactions[j - 1].summary.calendarOutgoDecimal);

                        //zaplanowane wydatki
                        $('#calendar_transactions_sum').text(transactions[j - 1].summary.calendarSum);
                        $('#calendar_transactions_sum_decimal').text(transactions[j - 1].summary.calendarSumDecimal);
                    }
                },
                eventAfterRender: function (event, element, view) {
                    var $el = $(element).find('.calendar-event');
                    $(element).css({
                        'width': $historyCalendar.find('.fc-day').eq(0).width(),
                        'padding-top': '2px',
                        'padding-bottom': '2px'
                    });

                    $el.each(function () {
                        eventPopup(element, event);
//                        eventTooltip($(this));
                    });
                },
                eventAfterAllRender: function () {
                    //ukrycie ostatniego tygodnia jesli jest pusty
                    var lastRow = $('.fc-week.fc-last', '#history_calendar');
                    if (lastRow.find('.fc-other-month').length === 7) {
                        lastRow.hide();
                    }
                }
            });

//            $(document).on('click', '.calendar-event-more', function (e) {
//                var $this = $(this);
//                $('.calendar-event-more').not($this).html('więcej <span class="highlight">&raquo;</span>').removeClass('active').siblings('.calendar-event.hide').slideUp();
//                if ($this.hasClass('active')) {
//                    $this.html('więcej <span class="highlight">&raquo;</span>').removeClass('active');
//                    $this.siblings('.calendar-event.hide').slideUp();
//                } else {
//                    $this.addClass('active');
//                    setTimeout(function () {
//                        $this.html('mniej <span class="highlight">&raquo;</span>').siblings('.calendar-event.hide').slideDown();
//                    }, 400);
//                }
//                zIndex++;
//                $historyCalendar.find('.calendar-event').not($this).closest('.fc-event').css('z-index', 10);
//                $this.closest('.fc-event').css('z-index', zIndex + 1);
//                zIndex += 1;
//                setTimeout(function () {
//                    $this.closest('.fc-event').css('z-index', zIndex);
//                    zIndex++;
//                    setTimeout(function () {
//                        $this.closest('.fc-event').css('z-index', zIndex);
//                        zIndex += 1;
//                    }, 300);
//                }, 200);
//            });

            // sterowanie kalendarzem - poprzedni/następny miesiąc
            $('#calendar_header_prev').on('click', function () {
                if (!$(this).hasClass('disabled')) {
                    var select = $('#calendar_header_select'),
                        selectedMonth = select.find('option:selected');

                    if (selectedMonth.index() !== 0) {
                        selectedMonth.prev().prop('selected', true);
                        select.trigger('change');
                    }
                }
            });
            $('#calendar_header_next').on('click', function () {
                if (!$(this).hasClass('disabled')) {
                    var select = $('#calendar_header_select'),
                        selectedMonth = select.find('option:selected'),
                        selectLength = select.find('option').length;

                    if (selectedMonth.index() < selectLength) {
                        selectedMonth.next().prop('selected', true);
                        select.trigger('change');
                    }
                }
            });

            // sterowanie kalendarzem - lista miesięcy
            $('#calendar_header_select').on('change', function (e) {
                var selectedMonth = Number($(this).find('option:selected').data('month')),
                    selectedYear = Number($(this).find('option:selected').data('year'));
                $('#history_calendar').fullCalendar('gotoDate', selectedYear, selectedMonth);
                $.uniform.update();

                if ($(this).find('option:selected').index() === 0) {
                    $('#calendar_header_prev').addClass('disabled');
                    $('#calendar_header_next').removeClass('disabled');
                } else if ($(this).find('option:selected').index() === ($(this).find('option').length - 1)) {
                    $('#calendar_header_prev').removeClass('disabled');
                    $('#calendar_header_next').addClass('disabled');
                } else {
                    $('#calendar_header_prev').removeClass('disabled');
                    $('#calendar_header_next').removeClass('disabled');
                }
            });

            $('#calendar_header_select').trigger('change');
        }
    });

    window.manager.define('app:modules:accounts:history:timeline:run', {
        on: function () {
            // pokazywanie i ukrywanie nagłówka "wydatki / dochody" po scrollu
            if ($('#history_timeline_head_wrapper').css('position') === 'fixed') {
                $(window).on('scroll resize', function () {
                    var $header = $('#history_timeline_head'),
                        $headerMinOffset = $('#history_timeline_header').offset().top,
                        scrolledPx = $(document).scrollTop();

                    if ($headerMinOffset < scrolledPx) {
                        $header.stop().fadeIn(200);
                    } else {
                        $header.stop().fadeOut(100);
                    }
                });
            }

            //doładowywanie transakcji po scrollu
            $(window).on('scroll', function () {
                var placer = $('#history_transaction_placer'),
                    //placerOffset = placer.offset().top,
                    loadIcon = placer.find('#load_icon'),
                    scrolledPx = $(document).scrollTop() + $(window).height() - 100,
                    downloadTransactions = function () {
                        var url = $('#history_account_filters').data('url'),
                            pageLast = $('.history-timeline-transaction-page', '#history_timeline').last().attr('data-page-last') || 'false',
                            data = {};

                        placer.attr('data-lock', 'disabled');
                        loadIcon.animate({'opacity': '1'}, 400);

                        //pobranie  transakcji
                        $.post(url, data, function (res) {
                            placer.before(res);
                        }).always(function (res) {
                            var response = res;
                            if (response.status !== 'undefined') {
                                if (response.status === 'moved') {
                                    window.location = response.location;
                                }
                            }
                            $('.category-select').not('[data-category-select="active"]').find('select').each(function () {
                                var $this = $(this);
                                if ($this.data('uniformed') === undefined && !$this.hasClass('nouniform')) {
                                    $this.uniform({
                                        selectAutoWidth: false,
                                        wrapperClass: $this.data('classes') || null
                                    });
                                }
                            });

                            $('.category-select').not('[data-category-select="active"]').categorySelect(window.categories, true);

                            // animacja doładowanych transakcji
                            setTimeout(function () {
                                $('.history-timeline-container, .history-timeline-box, .history-list-item', '#history_account').not(':visible').fadeIn(600);
                            }, 400);

                            // określenie czy strona jest ostatnią ze stron
                            pageLast = $('.history-timeline-transaction-page', '#history_timeline, #history_list').last().attr('data-page-last') || 'false';

                            loadIcon.animate({'opacity': '0'}, 400);

                            window.manager.require('app:modules:accounts:history:transactionsResults:run');

                            // jeśli strona nie jest ostatnią stroną odblokowujemy możliwość pobierania kolejnych wyników
                            if (pageLast !== 'true') {
                                setTimeout(function () {
                                    placer.attr('data-lock', 'active');
                                }, 1000);
                            }
                        });
                    };

                // sprawdzam ilość przeskrolowanych px i to czy pobieranie jest włączone (przez atrybut data-lock alementu placer)
                //if (placerOffset < scrolledPx && placer.attr('data-lock') === 'active') {
                 //   downloadTransactions();
                //}
            });
        }
    });

    window.manager.define('app:modules:accounts:downloadSummary:run', {
        on: function () {
            var container = $('#download_summary'),
                select = container.find('select'),
                link = container.find('#download_summary_link');

            link.on('click', function () {
                link.attr('href', select.find('option:selected').val());
            });
        }
    });


    window.manager.define('app:modules:accounts:setAccountName:run', {
        on: function () {

            var delegateAction = function (placeholder, input, switch_name, check_length) {
                var $text = $(placeholder),
                    $input = $(input),
                    $switch = $(switch_name);

                if ($input.val() === '') {
                    $switch.text('nadaj nazwę');
                } else {
                    $switch.text('zmień');
                }

                // po kliknięciu w link "zmień / nadaj nazwę" wysyłane jest zapytanie na adres wskazany w atrybucie data-url
                // po odpowiedzi 500 wyświetla się zmieniona nazwa
                // po błędzie połączenia lub odpowiedzi 400 wyświetla się komunikat o błędzie
                $switch.on('click', function (e) {
                    e.preventDefault();
                    var data,
                        url = $input.data('url'),
                        name = $input.val();

                    if (!$input.is(':visible')) {
                        $switch.text('zapisz');
                    } else {
                        $switch.text('zmień');
                    }

                    data = {
                        "name": name
                    };

                    if ($input[0].nodeName === 'SELECT') {
                        $input.parentsUntil('.form-row', '.field').toggleClass('hide');
                    } else {
                        $input.toggleClass('hide');
                    }
                    $text.removeClass('highlight').toggleClass('hide').text('');

                    if (check_length) {
                        if (name.length > 20) {
                            $text.text('nazwa może mieć max. 20 znaków').addClass('highlight');
                            return false;
                        }
                        if (name.length === 0) {
                            $text.text('nazwa nie może być pusta').addClass('highlight');
                            return false;
                        }
                    }

                    if (!$input.is(':visible')) {
                        $.post(url, data).success(function () {
                            $text.text($input.val());
                        }).fail(function () {
                            $text.text('wystąpił błąd').addClass('highlight');
                        });
                    }
                });
            };

            delegateAction.call(this, '#account_name_placeholder', '#account_name', '#account_name_switch', true);
            delegateAction.call(this, '#account_allocate_placeholder', '#account_allocate', '#account_allocate_switch', false);
        }
    });

    window.manager.define('app:modules:accounts:setDefaultAccount:run', {
        on: function () {
            var $trigger = $('#account_default_switch'),
                $info = $('#account_is_default_info'),
                $form = $('#account_is_default'),
                $value = $('#account_default_value'),
                url = $form.attr('action'),
                data = $form.data('url');

            // po kliknięciu w input typu radio wysyłane jest zapytanie na adres wskazany w atrybucie data-url kontenera
            // po błędzie połączenia lub odpowiedzi 400 wyświetla się komunikat o błędzie
            $trigger.on('click', function (e) {
                var $checked;

                e.preventDefault();
                $info.text('');
                $checked = $('input:checked', '#account_is_default');

                if (!$form.is(':visible')) {
                    $form.show();
                    $value.hide();
                    $trigger.text('zapisz');
                } else {
                    data = {
                        account_is_default : $checked.val()
                    };

                    $.post(url, data).success(function () {
                        $info.text('');
                        $value.show();
                        $form.hide();
                        $trigger.text('zmień');
                        $value.text($checked.data('value'));
                    }).fail(function () {
                        $info.text('wystąpił błąd');
                    });
                }
            });
        }
    });

    window.manager.define('app:modules:accounts:history:savingsSelect:run', {
        on: function () {
            var $container = $('#filter_savings_select'),
                $primary = $('.savings-select-primary', $container),
                $secondary = $('.savings-select-secondary', $container);

            $($primary).add($secondary).on('change', function () {
                var category = $primary.find('option:selected').val(),
                    subcategory = $secondary.find('option:selected').val(),
                    resultBox = $('#savings_select_list'),
                    resultList,
                    resultSelect = '',
                    i,
                    j,
                    $this;

                resultList = window.savingsFilter[category][subcategory];
                i = 0;
                j = resultList.length;
                if (j) {
                    resultSelect = '<select data-classes="selector-small selector-mt-small" name="savings_list" id="savings_list">';
                    for (i;  i <= j; i = i + 1) {
                        if (i === 0) {
                            resultSelect = resultSelect + '<option selected="" value="">wybierz</option>';
                        } else {
                            resultSelect = resultSelect + '<option value="' + resultList[i - 1].id + '">' + resultList[i - 1].name + '</option>';
                        }
                    }
                    resultBox.empty().append(resultSelect);

                    $this = $('#savings_list');
                    $this.uniform({
                        selectAutoWidth: false,
                        fileButtonHtml: $this.data('placeholder') || 'przeglądaj',
                        fileDefaultHtml: '',
                        wrapperClass: $this.data('classes') || null
                    });
                } else {
                    resultSelect = '<span class="filter-static-multiline highlight">brak oszczędności danego typu</span>';
                    resultBox.empty().append(resultSelect);
                }
            });

            $(document).on('change', '#savings_list', function () {
                var id = $(this).find('option:selected').val();
                if (typeof (id !== 'undefined') || id !== '') {
                    $('[type="submit"]', '#history_filter').trigger('click');
                }
            });
        }
    });
}());
/*global window, $, console, document, setTimeout, alert, isNaN */

(function () {
    'use strict';
    window.manager.define('app:modules:savings:calculator:run', {
        on: function () {
            var disableTimeSlider = function () {
                    if ($('#form_disable_time_slider').is(':checked')) {
                        $('#time_slider_container').css('opacity', '0.5');
                    } else {
                        $('#time_slider_container').css('opacity', '1');
                    }
                },
                sendForm,
                switchFilters;

            disableTimeSlider();

            $('#form_disable_time_slider').on('change', function () {
                disableTimeSlider();
            });

            $("#slider_amount").slider({
                range: 'min',
                min: 0,
                max: 100000,
                step: 1000,
                value: 10000,
                animate: true,
                slide: function (event, ui) {
                    if (ui.value < 200) {
                        ui.value = 200;
                    }
                    var text = ui.value.toString();
                    text = text.replace(/(?=(\d{3})+(?!\d))/g, " ");
                    $("#form_amount").val(text);
                }
            });
            $("#form_amount").on('change', function () {
                var is_nan, text = $(this).val();
                text = text.replace(" ", "");
                text = Number(text);
                if (text < 200) {
                    text = 200;
                }
                is_nan = isNaN(text);
                $("#slider_amount").slider({
                    value: text
                });

                if (text >= 1000) {
                    text = text.toString().replace(/(?=(\d{3})+(?!\d))/g, " ");
                }
                $("#form_amount").val(text);

                if (is_nan) {
                    text = '10 000';
                    $("#slider_amount").slider({
                        value: 10000
                    });
                    $("#form_amount").val(text);
                }
            });

            $("#slider_time").slider({
                range: 'min',
                min: 0,
                max: 36,
                step: 1,
                value: 12,
                animate: true,
                slide: function (event, ui) {
                    var monthText = '',
                        monthType = ui.value.toString();
                    monthType = monthType.substring(monthType.length - 1, monthType.length);
                    if (ui.value === 1) {
                        monthText = 'miesiąc';
                    } else if (ui.value >= 2 && ui.value <= 4) {
                        monthText = 'miesiące';
                    } else {
                        monthText = 'miesięcy';
                    }
                    if (ui.value >= 22 && (monthType === '2' || monthType === '3' || monthType === '4')) {
                        monthText = 'miesiące';
                    }

                    $("#time_text").text(ui.value + ' ' + monthText);
                    $("#form_months").val(ui.value);

                    if (ui.value === 0) {
                        $("#time_text").text('1 dzień');
                    }
                }
            });

            sendForm = function () {
                var $form = $('#savings_calc');

                $form.one('submit', function (e) {
                    e.preventDefault();
                    var data = $form.serialize(),
                        url = $form.attr('action');

                    $.post(url, data).success(function (res) {
                        var $container = $('#savings_chart'),
                            $tbody = $('#savings_table'),
                            savings = res.savings,
                            i = 0,
                            j = savings.length,
                            el,
                            column,
                            btnColorClass,
//                          //columnDesc,
                            tableRow,
                            promoClass,
                            line;

                        //czyszczenie kontenera
                        $container.empty();

                        if (j > 4) {
                            j = 4;
                        }

                        //tworzenie kolumn wykresu
                        for (i; i < j; i++) {
                            el = savings[i];

                            //nadanie koloru buttonów dla pierwszego linku
                            if (i !== 0) {
                                btnColorClass = 'gray';
                            } else {
                                btnColorClass = 'red arrow';
                            }

                            if (el.promo === "true") {
                                promoClass = 'promo';
                            } else {
                                promoClass = '';
                            }

                            //dodanie opisów oszczędności
//                            if (el.description.length) {
//                                columnDesc = '<small>' + el.description + '</small>';
//                            } else {
//                                columnDesc = '';
//                            }

                            column = '<div style="opacity: 0;" class="bar ' + promoClass + ' ' + el.columnHeight + '"><p><span>' + el.profit + '</span> PLN<br />' + el.name + '<br><small>' + el.period + '</small></p>' +
                                '<div class="tooltip"><div class="padding-10-0">zysk:<br /><b>' + el.profit + 'PLN</b></div><div class="grid-48">' + el.description + '</div></div>' +
                                '<a href="' + el.url + '" class="btn ' + btnColorClass + ' small">załóż</a></div>';
                            $container.append(column);

                            if (!$('.line.' + el.columnHeight, $container).length) {
                                line = '<div class="line ' + el.columnHeight + '" title="' + el.profit + '"></div>';
                                $container.append(line);
                            }
                        }

                        //animacja kolumn wykresu
                        for (i = 0; i < j; i++) {
                            $container.find('.bar').eq(i).delay(i * 100).animate({
                                'opacity': '1'
                            }, 200);
                        }

                        //czyszczenie tabeli
                        $tbody.empty();

                        //tworzenie tabeli
                        j = savings.length;
                        for (i = 0; i < j; i++) {
                            el = savings[i];
                            if (el.promo === "true") {
                                promoClass = ' class="promo"';
                                btnColorClass = 'red arrow';
                            } else {
                                promoClass = '';
                                btnColorClass = 'gray';
                            }
                            tableRow = '<tr' + promoClass + '><td class="no-br-right">' + el.tableName + '<span class="grid-ms-48 ms-show"><a class="btn small ' + btnColorClass + '" href="' + el.url + '">załóż</a></span></td><td class="no-br-left ms-hide"><a class="btn small ' + btnColorClass + '" href="' + el.url + '">załóż</a></td><td>' + el.period + '</td><td>' + el.interest + '<span class="ms-hide">%</span></td><td>' + el.profit + ' <small class="ms-hide">PLN</small></td></tr>';
                            $tbody.append(tableRow);
                        }
                    });
                    sendForm();
                });
            };
            sendForm();

            switchFilters = function () {
                $('#switch_filters').on('click', function (e) {
                    e.preventDefault();
                    var container = $('#filters_advanced');
                    container.slideToggle();
                    $(this).toggleClass('raquo-up', 'raquo-down');
                    if ($(this).find('small').text() === 'więcej kryteriów') {
                        $(this).find('small').text('mniej kryteriów');
                    } else {
                        $(this).find('small').text('więcej kryteriów');
                    }
                });
            };
            switchFilters();
        }
    });
}());
/*global window, $, console, document, setTimeout, AmCharts*/

(function () {
    'use strict';
    window.manager.define('app:modules:accountManager:filters:run', {
        on: function () {
            //filtry managera finansów
            var rangeType = $('#history_range_select option:selected').val(),
                dateRangeStart = $('#history_range_start').val(),
                dateRangeEnd = $('#history_range_end').val(),
                transactionType = $('#filter_transaction_types option:selected').val(),
                compareRangeType = $('#compare_range_select option:selected').val(),
                compareDateRangeStart = $('#compare_range_start').val(),
                compareDateRangeEnd = $('#history_range_end').val(),
                compareTransactionType = $('#filter_transaction_types option:selected').val();

            //czyszczenie wartości w filtrach
            $('#history_filter_advanced_reset').on('click', function (e) {
                e.preventDefault();
                $('#history_range_select option[value="' + rangeType + '"]').prop('selected', true);
                $('#history_range_start').val(dateRangeStart);
                $('#history_range_end').val(dateRangeEnd);
                $('#history_range_type1').trigger('click');
                $('#filter_transaction_types option[value="' + transactionType + '"]').prop('selected', true);

                $('#compare_range_select option[value="' + compareRangeType + '"]').prop('selected', true);
                $('#compare_range_start').val(compareDateRangeStart);
                $('#compare_range_end').val(compareDateRangeEnd);
                $('#compare_range_type1').trigger('click');
                $('#filter_transaction_types option[value="' + compareTransactionType + '"]').prop('selected', true);
                $('#filter_search_text').val('');
                $('#filter_type').val('');
                $('#history_filter_compare').prop('checked', false);

                $('.category-select-primary', '#filter_category_select').val($('.category-select-primary', '#filter_category_select option:first').val()).trigger('change');
                $.uniform.update();
            });

            $('#history_range_select').on('change', function () {
                $('#history_range_type1').prop('checked', true);
                $.uniform.update();
            });

            $('#history_range_start, #history_range_end').on('change', function () {
                $('#range_type2').prop('checked', true);
                $.uniform.update();
            });

            $('#history_filter_compare').on('change', function () {
                if ($(this).prop('checked') === true) {
                    $('#history_filter_more, #history_filter_more_2').slideDown();
                } else {
                    $('#history_filter_more, #history_filter_more_2').slideUp();
                }
            });

            //inicjalizacja datepickera dla daty początkowej zakresu historii transakcji
            $('#history_range_start').datepicker({
                dateFormat: window.ui_config.dpDateFormat,
                monthNames: window.ui_config.dpMonthNames,
                dayNamesMin: window.ui_config.dpDayNamesMin,
                maxDate: 0,
                onClose: function (selectedDate) {
                    $("#history_range_end").datepicker("option", "minDate", selectedDate);
                }
            });

            //inicjalizacja datepickera dla daty końcowej zakresu historii transakcji
            $('#history_range_end').datepicker({
                dateFormat: window.ui_config.dpDateFormat,
                monthNames: window.ui_config.dpMonthNames,
                dayNamesMin: window.ui_config.dpDayNamesMin,
                maxDate: 0,
                onClose: function (selectedDate) {
                    $("#history_range_start").datepicker("option", "maxDate", selectedDate);
                }
            });

            //inicjalizacja datepickera dla daty początkowej zakresu dat do porównania
            $('#compare_range_start').datepicker({
                dateFormat: window.ui_config.dpDateFormat,
                monthNames: window.ui_config.dpMonthNames,
                dayNamesMin: window.ui_config.dpDayNamesMin,
                maxDate: 0,
                onClose: function (selectedDate) {
                    $("#compare_range_end").datepicker("option", "minDate", selectedDate);
                }
            });

            //inicjalizacja datepickera dla daty końcowej zakresu dat do porównania
            $('#compare_range_end').datepicker({
                dateFormat: window.ui_config.dpDateFormat,
                monthNames: window.ui_config.dpMonthNames,
                dayNamesMin: window.ui_config.dpDayNamesMin,
                maxDate: 0,
                onClose: function (selectedDate) {
                    $("#compare_range_start").datepicker("option", "maxDate", selectedDate);
                }
            });

            // otwieranie datepickerów po kliknięciu w ikonę kalendarza
            $('.history-account-settings  .i-calendar, .history-account-settings-advanced .i-calendar').on('click', function () {
                $(this).siblings('.history-range-input').trigger('focus');
            });

            //rozwijanie filtrów zaawansowanych
            $(document).on('click', '#history_filter_switch', function (e) {
                e.preventDefault();
                $('#history_filter_advanced').stop().slideToggle(600);
                $(this).find('.raq-icon').toggleClass('raquo-down raquo-up');
            });

            //rozwijanie filtrów zaawansowanych dla mobile smartphone
            $(document).on('click', '#history_filter_switch_ms', function (e) {
                e.preventDefault();
                $('#history_filter_advanced').add('.history-row-advanced-ms', '#history_filter').stop().slideToggle(600);
                $(this).find('.raq-icon').toggleClass('raquo-down raquo-up');
            });

            // reset widoczności elementów filtra w zależności od przedziału szerokości okna w RWD
            $(window).on('resize', function () {
                var windowWidth = $(window).width(),
                    $form = $('#history_account_filters');

                if ($form.hasClass('filters-ms')) {
                    if (windowWidth >= 768) {
                        $form.find('.history-row-advanced-ms, .history-account-settings-advanced').removeAttr('style');
                        $('.raquo-up', '#history_filter_switch_ms, #history_filter_switch').addClass('raquo-down').removeClass('raquo-up');
                    }
                } else {
                    if (768 > windowWidth) {
                        $form.find('.history-row-advanced-ms, .history-account-settings-advanced').removeAttr('style');
                        $('.raquo-up', '#history_filter_switch_ms, #history_filter_switch').addClass('raquo-down').removeClass('raquo-up');
                    }
                }

                if (768 < windowWidth) {
                    $form.removeClass('filters-ms');
                } else {
                    $form.addClass('filters-ms');
                }
            });

            $(window).on('load', function () {
                var windowWidth = $(window).width(),
                    $form = $('#history_account_filters');

                if (768 < windowWidth) {
                    $form.removeClass('filters-ms');
                } else {
                    $form.addClass('filters-ms');
                }
            });

        }
    });

    window.manager.define('app:modules:accountManager:tabs:run', {
        on: function () {
            // przełączanie zakładek z wykresami kołowymi dochodów i wydatków w rozdzielczości smartphone
            var $container = $('#financial_manager_pie'),
                $tabs = $container.find('.financial-manager-label'),
                $boxes = $container.find('.financial-manager-pie-box'),
                $containerCompare = $('#financial_manager_pie_compare'),
                $tabsCompare = $containerCompare.find('.financial-manager-label'),
                $boxesCompare = $containerCompare.find('.financial-manager-pie-box');

            $tabs.on('click', function () {
                var $index = $(this).index();
                $tabs.removeClass('active').eq($index).addClass('active');
                $boxes.removeClass('active').eq($index).addClass('active');
            });

            $tabsCompare.on('click', function () {
                var $index = $(this).index();
//                console.log($index);
                $tabsCompare.removeClass('active').eq($index).addClass('active');
                $boxesCompare.removeClass('active').eq($index).addClass('active');
            });

            return true;
        }
    });
}());
/*global window, $, console, document, setTimeout, alert */

(function () {
    'use strict';
    window.manager.define('app:modules:loginForm:run', {
        on: function () {

//            $('#login_password, #login_token').on('keyup change', function () {
//                if ($('#login_password').val().length >= 8 && $('#login_token').val().length >= 6) {
//                    $('#login_next').prop('disabled', false);
//                } else {
//                    $('#login_next').prop('disabled', true);
//                }
//            });

            var $form = $('#login_form'),
                url = $form.attr('action'),
                status = $form.data('status'),
                data;

            //obsługa przycisku "dalej / zaloguj się"
            $('#login_id, #login_password, #login_token').on('keyup change', function () {
                if ($('#login_id').val().length >= 8 && $('#login_next').prop('disabled')) {
                    $('#login_next').prop('disabled', false);
                } 
				if  ($('#login_id').val().length < 8) {
                    $('#login_next').prop('disabled', true);
                }
                if ($form.data('status') === "step_1") {
                    $("#login_next").text('dalej');
                }
                if ($form.data('status') === "step_2") {
                    $("#login_next").text('zaloguj się');
                    if ($('#login_password').val().length >= 8) {
                        $('#login_next').prop('disabled', false);
                    } else {
                        $('#login_next').prop('disabled', true);
                    }
                } else if ($form.data('status') === "step_2_token") {
                    $("#login_next").text('zaloguj się');
                    if ($('#login_password').val().length >= 8 && $('#login_token').val().length >= 6) {
                        $('#login_next').prop('disabled', false);
                    } else {
                        $('#login_next').prop('disabled', true);
                    }
                }
            });
            $('#login_id, #login_password, #login_token').on('keyup change', function () {
                if (/\s/g.test($(this).val())) {
                    var start = this.selectionStart,
                        end = this.selectionEnd;
                    $(this).val($(this).val().replace(/\s/g, ''));
                    this.setSelectionRange(start, end);
                }
            });

            $('#login_id').focus();

            setTimeout(function () {
                $('#login_id').on('focus', function () {
                    if (status !== 'step_1') {
                        window.location.reload();
                    }
                });
                $('#kb_login_id').on('click', function () {
                    if (status !== 'step_1') {
                        window.location.reload();
                    }
                });
            });

            $form.on('submit', function (e) {
                e.preventDefault();
                data = {};
                setTimeout(function () {
                    if (!$form.find('.has-error').not('.ignore-error').length) {
                        if (status === 'step_1') {
                        	var $identyfikator = $('#login_id').val();
                            data = {
                                'step': status,
                                'id': $('#login_id').val()
                            };
                            $.post(url, data).success(function (res) {
                                var responseStatus = res.status;
                                $form.data('status', responseStatus);
                                status = $form.data('status');
                                if (responseStatus === 'step_2') {
                                    $form.data('status', 'step_2');
                                    $('#login_id').css('opacity', '0.5');
                                    $('#login_password_container').removeClass('hide').find('.has-error').removeClass('has-error');
                                    $('#login_password_container').find('.error').remove();
                                    $('#login_next').prop('disabled', true).text('zaloguj się');
                                    window.location = res.url + '?login=' + $identyfikator;
                                } else if (responseStatus === 'step_2_token') {
                                    $form.data('status', 'step_2_token');
                                    $('#login_id').css('opacity', '0.5');
                                    $('#login_password_container, #login_token_container').removeClass('hide').find('.has-error').removeClass('has-error');
                                    $('#login_password_container, #login_token_container').find('.error').remove();
                                    $('#login_next').prop('disabled', true).text('zaloguj się');
                                } else if (responseStatus === 'success' || responseStatus === 'error') {
                                    window.location = res.url;
                                }
                            });
                        } else if (status === 'step_2') {
                        	data = {
                                'step': status,
                                'id': $('#login_id').val()
                            };
                            if (!$('#login_password_container').find('.has-error').length) {
                                $.post(url, data).success(function (res) {
                                    var responseStatus = res.status;
                                    $form.data('status', responseStatus);
                                    if (responseStatus === 'success' || responseStatus === 'error') {
                                        window.location = res.url;
                                    }
                                });
                            }
                        } else if (status === 'step_2_token') {
                            data = {
                                'step': status,
                                'id': $('#login_id').val(),
                                'token': $('#login_token').val()
                            };
                            if (!$('#login_password_container, #login_token_container').find('.has-error').length) {
                                $.post(url, data).success(function (res) {
                                    var responseStatus = res.status;
                                    $form.data('status', responseStatus);
                                    if (responseStatus === 'success' || responseStatus === 'error') {
                                        window.location = res.url;
                                    }
                                });
                            }
                        }

                    }
                }, 100);
            });

            // uruchomienie klawiatury ekranowej
            $('.login-kb', '#login_form').click(function () {
                var keyboardId = $(this).data('id'),
                    keyboard = $('#' + keyboardId),
                    options = {
                        maxLength: $('#' + keyboardId).attr('maxlength'),
                        display: {
                            'bksp'   :  "\u2190",
                            'accept' : 'akceptuj',
                            'cancel' : 'anuluj'
                        },
                        hidden: function () {
                            keyboard.getkeyboard().destroy();
                        }
                    };

                $('html, body').stop().animate({
                    scrollTop: $(document).scrollTop()
                }, 100);

                if (keyboardId === 'login_token') {
                    options.layout = 'custom';
                    options.customLayout = {
                        'default' : [
                            '0 1 2 3 4',
                            '5 6 7 8 9',
                            '{accept} {cancel} {bksp}'
                        ]
                    };
                }

                keyboard.keyboard(options);
                keyboard.getkeyboard().reveal();
                $('#' + keyboardId).on('focus', function () {
                    keyboard.getkeyboard().reveal();
                });
            });
        }
    });
}());
/*global window, $, console, setTimeout, alert, clearTimeout, Validator, document */

(function () {
    'use strict';

    // obsługa skrzynki odbiorczej i nadawczej
    window.manager.define('app:modules:contact:contactInbox:run', {
        on: function () {
            var $form = $('#contact_inbox'),
                url = $form.data('load');

            $form.on('click', '.table-header', function (e) {
                var $this = $(this),
                    requestData = {
                        'id': $this.data('id')
                    },
                    detailsSection = $this.siblings('.table-details'),
                    loaderIcon = $this.siblings('.table-details-loader'),
                    $visible_details,
                    $id = $this.data('id');

                if ($(e.target).attr('href') === undefined && e.target.nodeName !== 'INPUT' && !$this.hasClass('no-hover')) {
                    $visible_details = $('.table-details:visible', $form);
                    if ($visible_details.length) {
                        $visible_details.siblings('.table-header').removeClass('active');
                        $visible_details.slideUp();
                    }
                    if ($this.hasClass('table-header-enabled')) {
                        loaderIcon.slideDown();
                        $.post(url + $id + '.html', requestData)
                            .success(function (res) {
                                $this.removeClass('table-header-enabled');
                                $('.contact-message-new', $this).removeClass('contact-message-new');
                                detailsSection.empty().append(res).slideDown();
                                loaderIcon.remove();
                                $this.addClass('active');
                            }).fail(function () {
                                $this.removeClass('table-header-enabled');
                                detailsSection.empty().append('<p class="text-center padding-15-0">wystąpił błąd</p>').slideDown();
                                loaderIcon.remove();
                                $this.addClass('active');
                            });
                    } else {
                        detailsSection.stop().slideToggle(function () {
                            if (detailsSection.is(':visible')) {
                                $this.addClass('active');
                            }
                        });
                    }
                }
            });

            $('#contact_remove_messages').on('click', function (e) {
                e.preventDefault();

                if ($('input[type="checkbox"]:checked', $form).length) {
                    $('#contact_remove_dialog').dialog({
                        resizable: false,
                        draggable: false,
                        height: 160,
                        modal: true,
                        buttons: {
                            Ok: {
                                click: function () {
                                    var $dialog = $(this);
                                    $dialog.dialog("close");
                                    $form.submit();
                                },
                                'text': 'Tak',
                                'class': 'btn red small'
                            },
                            Cancel: {
                                click: function () {
                                    var $dialog = $(this);
                                    $dialog.dialog("close");
                                },
                                'text': 'Nie',
                                'class': 'btn gray small'
                            }
                        }
                    });
                }
            });
        }
    });

    // popup zamówienia kontaktu  z doradcą
    window.manager.define('app:modules:contact:adviser:run', {
        on: function () {

            var $btn = $('#contact_adviser'),
                $chat = $('#contact_chat');

            $btn.on('click', function (e) {
                e.preventDefault();
                window.manager.require('app:modules:contact:adviser:show');
            });

            $chat.on('click', function (e) {
                e.preventDefault();
                window.manager.require('app:modules:contact:chat:show');
            });


            // pokazywanie popupa "kontakt z doradcą"
            window.manager.define('app:modules:contact:adviser:show', {
                on: function () {
                    var oShadowbox;

                    oShadowbox = new window.ShadowBox({
                        ajax: 'brak_funkcjonalnosci.html',
                        classes: 'contact-popup',
                        callback: function () {
                            

                            $(document).on('click', '.shadowbox-close, .shadowbox_overlay', function (e) {
                                e.preventDefault();
                                $('#shadowbox').remove();
                            });
                        }
                    });
                    return oShadowbox;
                }
            });

            // pokazywanie popupa "kontakt z doradcą"
            window.manager.define('app:modules:contact:chat:show', {
                on: function () {
                    var oShadowbox;

                    oShadowbox = new window.ShadowBox({
                        ajax: 'brak_funkcjonalnosci.html',
                        classes: 'contact-popup',
                        callback: function () {
                            

                            $(document).on('click', '.shadowbox-close', function (e) {
                                e.preventDefault();
                                $('#shadowbox').remove();
                            });
                        }
                    });
                    return oShadowbox;
                }
            });
        }
    });
}());
/*global window, $, console */

(function () {
    'use strict';

    window.manager.define('app:modules:ie8:run', {
        on: function () {
            $('.grid-24:nth-child(2n+1)', $('.accounts-widgets')).css({
                marginLeft: 0
            });
            $('.grid-24:nth-child(2n+1)', $('#widgets_manager')).css({
                marginLeft: 0
            });
        }
    });
}());