/*global $, window, console, alert, Modernizr, navigator, document, setTimeout */

(function () {
    'use strict';

    Modernizr.addTest('ie8', function () {
        return !!navigator.userAgent.match(/msie 8\.0/i);
    });

    Modernizr.addTest('ie9', function () {
        return !!navigator.userAgent.match(/msie 9\.0/i);
    });

    // utworzenie obiektu Validator'a
    window.oValidator = new window.Validator({
        // gdy pole poprawne
        addValid: function (opts) {
            var $field;
            if (opts.validator_message_target !== null) {
                if (opts.validator_message_target.split('|').length > 1) {
                    $(opts.validator_message_target.split('|')[0]).slideUp();
                } else {
                    $(opts.validator_message_target).empty();
                }
            } else {
                $field = opts.item.parentsUntil('.form-row', '.field');
                $field.removeClass('has-error').addClass('is-valid');
//                $('div.error', $field).slideUp(200, function () {
                $('div.error', $field).remove();
//                });
            }
        },
        // gdy pole niepoprawne
        addInvalid: function (opts) {
            var $field;

            $field = opts.item.parentsUntil('.form-row', '.field');
            opts.item.removeClass('has-error');
            $field.addClass('has-error').removeClass('is-valid');

            if (opts.validator_message_target !== null) {
                if (opts.validator_message_target.split('|').length > 1) {
                    $(opts.validator_message_target.split('|')[0]).stop(true, false).slideDown();
                    $('.msg', $(opts.validator_message_target.split('|')[0])).html(opts.validator_message);
                } else {
                    $(opts.validator_message_target).html('<div class="error hide">' + opts.validator_message + '</div>');
                    $('.error', $(opts.validator_message_target)).stop(true, false).slideDown();
                }
            } else {
                if ($('div.error', $field).length) {
                    $('div.error', $field).stop(true, false).slideUp(200, function () {
                        $(this).remove();
                        $field.append('<div class="error" style="display: none">' + opts.validator_message + '</div>');
                        $('.error', $field).stop(true, false).slideDown(200);
                    });
                } else {
                    $field.append('<div class="error" style="display: none">' + opts.validator_message + '</div>');
                    $('.error', $field).stop(true, false).slideDown(200);
                }

            }
        },
        // gdy pole neutralne, a nie ma wymaganego pola, usunięcie błędu lub poprawności
        addClear: function (opts) {
            var $field;
            if (opts.validator_message_target !== null) {
                if (opts.validator_message_target.split('|').length > 1) {
                    $(opts.validator_message_target.split('|')[0]).slideUp();
                } else {
                    $(opts.validator_message_target).empty();
                }
            } else {
                $field = opts.item.parentsUntil('.form-row', '.field');
                $field.removeClass('has-error is-valid');
//                $('div.error', $field).slideUp(200, function () {
                $('div.error', $field).remove();
//                });
            }
        }
    });

    // główny config dla całej aplkacji
    window.ui_config = {
        // format daty dla jQuery datepicker
        dpDateFormat: 'dd.mm.yy',
        // nazwy miesięcy dla jQuery datepicker
        dpMonthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
        // nazwy skrócone dni dla jQuery datepicker
        dpDayNamesMin: ["Nd", "Pn", "Wt", "Śr", "Cz", "Pt", "So"]
    };


    window.operators = {
        'operator_1': {
            label: 'Simplus',
            type: 'range',
            min: 5,
            max: 150
        },
        'operator_3': {
            label: 'T-mobile',
            type: 'range',
            min: 5,
            max: 500
        },
        'operator_4': {
            label: 'Orange',
            type: 'list',
            values: ['5', '10', '25', '40', '50', '100', '200']
        },
        'operator_6': {
            label: 'Sami Swoi',
            type: 'list',
            values: ['10', '20', '40', '80', '100', '160']
        },
        'operator_2': {
            label: 'MixPlus',
            type: 'range',
            min: 5,
            max: 150
        },
        'operator_7': {
            label: 'Play',
            type: 'range',
            min: 5,
            max: 300
        },
        'operator_8': {
            label: '36i6',
            type: 'range',
            min: 5,
            max: 150
        },
        'operator_9': {
            label: 'GaduAIR',
            type: 'list',
            values: ['10', '20', '50', '100']
        },
        'operator_10': {
            label: 'Polsat Cyfrowy',
            type: 'range',
            min: 5,
            max: 500
        },
        'operator_12': {
            label: 'Virgin Mobile',
            type: 'list',
            values: ['5', '10', '30', '50', '100', '150']
        }
    };

    $(function () {

        // uruchomienie uniform (fancy select, checkbox, radiobutton
        if ($('select, input[type="checkbox"], input[type="radio"], input[type="file"]').length) {
            window.manager.require('app:modules:uniform:run');
        }

        if ($('#mobile_nav_wrapper').length) {
            window.manager.require('app:modules:navMobile:secondMenu:run');
        }

        // delegacja głównego menu w mobile
        window.manager.require('app:modules:navMobile:menuTrigger:run');
        // delegacja okienka wyszukiwarki w mobile
        window.manager.require('app:modules:navMobile:searchTrigger:run');
        if ($('.menu-list').length) {
            window.manager.require('app:modules:navMobile:subMenu:run');
        }
        // delegacja zdarzeń na kontach na dashboardzie - zwijanie i rozwijanie widgetów konta,
        // oraz kliknięcie w wiersz widgetu historii przekierowuje na wskazany link historii.
        if ($('#accounts_list').length) {
            window.manager.require('app:modules:accounts:dashboardItem:run');
            window.manager.require('app:modules:accounts:widgetHistory:rowClick:run');
//            window.manager.require('app:modules:accounts:widgetFinancialManager:run');
        }

        // lista kont - przekierowania do szczegółów danego konta
//        if ($('#accounts').length) {
//            window.manager.require('app:modules:accounts:linkToDetails:run');
//        }

        
        if ($('.print2').length) {
	        $('.print2').on('click', function (e) {
	        	e.preventDefault();
	        	window.manager.require('app:modules:modal:window:show');
	        }); 
        }
        
        
        //obsługa filtrów historii operacji - zwijanie i rozwijanie filtrów zaawansowanych,
        // wysyłanie formularza
        if ($('#history_account').length) {
            window.manager.require('app:modules:accounts:history:filters:run');
        }

        // zmiana nazwy własnej konta
        if ($('#account_name_switch').length || $('#account_allocate_switch').length) {
            window.manager.require('app:modules:accounts:setAccountName:run');
        }

        // ustalanie domyślnych kont
        if ($('#account_is_default').length) {
            window.manager.require('app:modules:accounts:setDefaultAccount:run');
        }

        //obsługa osi czasu w historii konta
        if ($('#history_timeline_head, #history_list').length) {
            window.manager.require('app:modules:accounts:history:timeline:run');
        }

        //obsługa kalendarza w historii konta
        if ($('#history_calendar').length) {
            window.manager.require('app:modules:accounts:history:calendar:run');
        }

        // delegacja zdarzenia kliknięcia na wiersz z lokatami, kredytami itp. na dashboardzie
        if ($('.account-details[data-href]').length) {
            window.manager.require('app:modules:accounts:dashboardItemAsLink:run');
        }

        //obsługa pobierania podsumowania transakji wg
        if ($('#download_summary').length) {
            window.manager.require('app:modules:accounts:downloadSummary:run');
        }

        // moduł zarządzania pulpitem
        if ($('#dashboard_manager').length) {
            window.manager.require('app:modules:dashboardManager:run');
        }

        // manager konta - wykres kolumnowy - obsługa filtrów
        if ($('#financial_manager_columns').length) {
            window.manager.require('app:modules:accountManager:filters:run');
        }

        // manager konta - wykres kołowy - obsługa filtrów - tabów
        if ($('#financial_manager_pie').length) {
            window.manager.require('app:modules:accountManager:filters:run');
            window.manager.require('app:modules:accountManager:tabs:run');
        }

        // kalkulator oszczędności
        if ($('#savings_calc').length) {
            window.manager.require('app:modules:savings:calculator:run');
        }

        // logowanie do systemu
        if ($('#login_form').length) {
            window.manager.require('app:modules:loginForm:run');
        }
        if ($('#send_select').length) {
            window.manager.require('app:modules:accounts:dashboardItemAsLink:run');
        }

        if ($('.more-details-container', '#aggregation_list').length) {
            window.manager.require('app:modules:ui:showMoreDetails:run');
        }

        if ($('.offer-box').length) {
            window.manager.require('app:modules:ui:changeBackgrounds:run');
        }

        // komunikat przy potwierdzeniu edycji kart o zapisie nowego nr tel
        if ($('#cards-success').length) {
            window.manager.require('app:modules:cards:run');
        }

        if ($('#contact_inbox').length) {
            window.manager.require('app:modules:contact:contactInbox:run');
        }

        if ($('#contact_adviser').length) {
            window.manager.require('app:modules:contact:adviser:run');
        }

        // obsługa formularzy
        window.manager.require('app:modules:forms:run');

        // obsługa UI
        window.manager.require('app:modules:ui:run');

        // wsparcie dla IE8
        if ($.browser.msie && $.browser.version < 9) {
            window.manager.require('app:modules:ie8:run');
        }

        // fallback dla lte IE9 dot. placeholderów.
        $('input[placeholder]').makePlaceholder();

        $(window).on('resize', function () {
            var $el, width;

            width = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;

            $el = $('input.hasDatepicker');

            if (width < 768) {
                $el.prop('readonly', true);
            } else {
                $el.prop('readonly', false);
            }
        });
        $(window).trigger('resize');
        setTimeout(function () {
            $(window).trigger('resize');
        }, 200);
    });
}());