$(function() {
	$("#header_placeholder").load("wspolne_header.html", function() {
		window.manager.require('app:modules:navMobile:menuTrigger:run');
	});
	$("#footer_placeholder").load("wspolne_footer.html");
	loadAuthorizationBox();
});

function loadAuthorizationBox() {
	var operation = $('#authorization_placeholder').data('operation');
	$("#authorization_placeholder").load("wspolne_autoryzacja.html", function() {
		$('#token-desc').html(operation);
	});
}